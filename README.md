# ImportManager

ImportManager consists of two parts, one is the `importmanager` Python library
itself, the other is the ImportManager CLI (`imcli`) that can be used by the
user to import new data into the histHub graph database.

## Set up

As a user you usually don't need to worry about the `importmanager` library.
Just install the CLI:

```sh
pip install imcli@'git+https://gitlab.com/histhub/norm/importmanager.git#subdirectory=frontends/cli'
```

Before you run the CLI, you must create a config file named `.importmanager.ini`
either in the current working directory or your home folder.
You can use the provided [importmanager.ini.example](./importmanager.ini.example)
as a template.

## Develop

Developers are encouraged to set up a development environment in a virtualenv.
To do so, you can execute `make virtualenv`, and then activate it using `. venv/bin/activate`.

It already includes editable installations of the ImportManager library and the
available frontends (ImportManager CLI). This is to ensure that you always use the
latest version of the code as you continue to work on the code.

## Initialise (and upgrade) ImportManager database schema.

In addition to the histHub graph database, ImportManager uses its own separate
database to store state information, linking decisions, logs about the imports.

### PostgreSQL

There are SQL migration scripts available.
After having checked out the source code and set up the environment to run
ImportManager, execute the `./db/migrate.py` script in the root of the source
code repository to initialise the ImportManager database.

e.g.:
```bash
cd <importmanager source folder>
./db/migrate.py postgresql://postgres@/histhub_staging --migrate
```

NB: The ImportManager database should be put into a separate database as it
creates temporary schemas when imports are performed.  
While it may be possible to share a database with the histHub graph database,
it is not officially supported to do so and bugs resulting of such a set up will
not be fixed.  
The database URL is a
[SQLAlchemy-like](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine)
URL string.
