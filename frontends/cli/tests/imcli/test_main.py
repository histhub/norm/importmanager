import configparser
import os
import tempfile
import unittest

from typing import AnyStr
from unittest.mock import (patch, _patch)

from imcli.main import (_find_config, _load_config, DictCommandManager, IMCLI)

IMCLI._init()


class DemoClass1:
    pass


class DemoClass2:
    pass


class FindConfigTest(unittest.TestCase):
    @patch("os.path.isfile")
    def test_search_order(self, isfile_mock: _patch) -> None:
        correct_order = (x for x in (  # we need a generator
            os.path.join(os.curdir, ".importmanager.ini"),
            os.path.join(os.path.expanduser("~"), ".importmanager.ini")
        ))

        def _isfile(p: AnyStr) -> bool:
            assert os.path.abspath(p) == os.path.abspath(next(correct_order))
            return False  # we want to check the order
        isfile_mock.side_effect = _isfile  # type: ignore

        _find_config()

    @patch("os.access")
    @patch("os.path.isfile")
    def test_no_config_found(self, isfile_mock, access_mock):
        isfile_mock.return_value = False
        assert _find_config() is None
        assert not access_mock.called

    @patch("os.access")
    @patch("os.path.isfile")
    def test_find_config(self, isfile_mock, access_mock):
        access_mock.side_effect = lambda p, m: True
        isfile_mock.side_effect = lambda p: True

        config_path = _find_config()
        assert str == type(config_path)
        assert config_path is not None


class LoadConfigTest(unittest.TestCase):
    DEFAULTS = {
        configparser.DEFAULTSECT: {
            "_default": "true"
        }
    }

    def test_no_config(self) -> None:
        assert dict(DEFAULT=dict()) == _load_config()

    def test_no_path(self) -> None:
        assert self.DEFAULTS == _load_config(None, self.DEFAULTS)

    def test_empty_config(self) -> None:
        with tempfile.TemporaryFile() as f:
            assert self.DEFAULTS == _load_config(f, self.DEFAULTS)

    def test_config(self) -> None:
        with tempfile.TemporaryFile(mode="w+", encoding="utf-8") as f:
            f.write("""
            [DEFAULT]
            _default = false
            _config = true
            [config]
            foo = foo
            """)
            f.seek(0, os.SEEK_SET)

            config = _load_config(f, self.DEFAULTS)
            config_dict = {k: dict(v) for k, v in config.items()}

            assert config_dict == {
                "DEFAULT": {
                    "_default": "false",
                    "_config": "true"
                },
                "config": {
                    "foo": "foo",
                    "_default": "false",
                    "_config": "true"
                }
            }

    @patch("imcli.main.LOG")
    def test_config_no_section(self, logger_mock):
        with tempfile.TemporaryFile(mode="w+", encoding="utf-8") as f:
            f.write("""
            _default = false
            _config = true
            """)
            f.seek(0, os.SEEK_SET)

            try:
                _load_config(f, self.DEFAULTS)
            except Exception as e:
                raise AssertionError("_load_config should not raise") from e
            assert logger_mock.error.called


class DictCommandManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        self._commands = {
            "demo one": ("%s:DemoClass1" % __name__),
            "demo_two": ("%s:DemoClass2" % __name__)
        }

    @patch("imcli.main.DictCommandManager._load_commands")
    def test_init(self, load_commands_mock: _patch) -> None:
        DictCommandManager(self._commands)
        load_commands_mock.assert_called_with(self._commands)  # type: ignore

    def test_load_commands(self) -> None:
        cm = DictCommandManager(self._commands, convert_underscores=True)

        assert "demo one" in cm.commands
        assert cm.commands["demo one"].load() is DemoClass1
        assert "demo two" in cm.commands
        assert cm.commands["demo two"].load() is DemoClass2

    def test_load_commands_no_conversion(self) -> None:
        cm = DictCommandManager(self._commands, convert_underscores=False)

        assert "demo one" in cm.commands
        assert cm.commands["demo one"].load() is DemoClass1
        assert "demo two" not in cm.commands
        assert "demo_two" in cm.commands
        assert cm.commands["demo_two"].load() is DemoClass2

    def test_add_command(self) -> None:
        cm = DictCommandManager(dict())
        cm.add_command("foo", DemoClass1)

        foocomm = cm.commands["foo"]
        assert hasattr(foocomm, "load")
        assert foocomm.load() is DemoClass1

    def test_load_commands_namespace(self) -> None:
        cm = DictCommandManager({})
        self.assertRaises(AttributeError, cm.load_commands, None)

    def test_add_legacy_command(self) -> None:
        cm = DictCommandManager({})
        self.assertRaises(AttributeError, cm.add_legacy_command,
                          old_name="", new_name="")


class IMCLITest(unittest.TestCase):
    def test_singleton(self) -> None:
        assert IMCLI.instance() is not None
        assert IMCLI().instance() is IMCLI.instance()
        assert IMCLI().instance() is IMCLI().instance()
