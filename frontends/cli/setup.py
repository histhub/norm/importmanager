import os

from setuptools import setup, find_packages


try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements  # noqa: F401
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


base = os.path.abspath(os.path.dirname(__file__))


def read_file(path):
    """Read file and return contents"""
    try:
        with open(path, encoding="utf-8") as f:
            return f.read()
    except FileNotFoundError:
        return ""


def get_reqs():
    """Parse requirements.txt file and return array"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.txt"), session="hack")
    return [
        r.requirement if hasattr(r, "requirement") else str(r.req)
        for r in reqs]


def get_meta(name):
    """Get metadata from imcli/__init__.py"""
    import imcli
    return getattr(imcli, name)


setup(
    name="imcli",
    version=get_meta("__version__"),
    license=get_meta("__license__"),
    author=get_meta("__author__"),

    description=get_meta("__description__"),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Intended Audience :: Developers",
        "Environment :: Console",
        "Typing :: Typed"
    ],

    # platforms=["mac", "linux"],

    # scripts=[],

    # provides=[],
    install_requires=get_reqs(),

    # namespace_packages=[],
    packages=find_packages(exclude=["imcli.tests"]),
    python_requires="~=3.4",
    # include_package_data=True,

    entry_points={
        "console_scripts": [
            "imcli = imcli.main:main"
        ]
    },
    zip_safe=True)
