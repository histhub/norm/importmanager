SRC_DIR := ./imcli

.PHONY: clean install_reqs lint test type_check

.PRECIOUS: $(VENV)/.fe_cli_reqs
$(VENV)/.fe_cli_reqs: requirements.txt | $(VENV)
# Install requirements for the CLI frontend
	$(PIP_INSTALL) -r ./requirements.txt
	@touch '$@'


type_check:
	$(VENVPYTHON) -m mypy --strict --python-version $(MIN_PYTHON_VERSION) --allow-subclassing-any --allow-untyped-decorators --ignore-missing-imports --allow-untyped-calls ./imcli

install_reqs: $(VENV)/.fe_cli_reqs

lint: | $(VENV)/.fe_cli_reqs
	$(VENVPYTHON) -m flake8 --show-source '$(SRC_DIR)'

test: | $(VENV)/.fe_cli_reqs $(VENV)/.development_reqs
	$(VENVPYTHON) -m coverage run --branch -m unittest discover --verbose -p 'test_*.py' tests.imcli
	$(VENVPYTHON) -m coverage report --show-missing --include='imcli/*'

clean:
	-$(RM) .coverage
	-$(RM) -R .mypy_cache
	-$(RM) -R imcli.egg-info
	-find '$(SRC_DIR)' -depth -iname '*.pyc' -exec $(RM) -R '{}' \;
	-find '$(SRC_DIR)' -depth -type d -name '__pycache__' -exec $(RM) -R '{}' \;
