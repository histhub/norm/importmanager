import argparse
import collections.abc
import configparser
import importlib
import logging
import os
import sys

from cliff.app import App
from cliff.command import Command
from cliff.commandmanager import (EntryPointWrapper, CommandManager)
from typing import (
    Dict, Iterable, List, NoReturn, Optional, Tuple, Union, TYPE_CHECKING)
from eavlib.connection import EAVConnection
from eavlib.graph import Graph
from eavlib.db.url import make_url
from normlib.db.sidecar_db import SidecarDBConnection

from importmanager.db.state_db import StateDBConnection

from imcli import (__description__, __version__)
from imcli.errors import ConfigError

if TYPE_CHECKING:
    import pkg_resources

CONFIG_DEFAULTS = {
    "importmanager": {
        "logformat": "%(levelname)-8s %(asctime)s %(message)s"
        # "model": ""
        },
    "histhub_db": {
        # "url": "...",
        "model_name": "histHub"
        },
    "sidecar_db": {
        # "url": "..."
        },
    "state_db": {
        # "url": "..."
        }
    }  # type: Dict[str, Dict[str, str]]

COMMANDS = {
    # Import jobs
    "import create": "imcli.commands.job:CreateImport",
    "import list": "imcli.commands.job:ListImports",
    "import show": "imcli.commands.job:ShowImport",
    "import cancel": "imcli.commands.job:CancelImport",
    # Import providers
    "provider create": "imcli.commands.provider:CreateProvider",
    "provider list": "imcli.commands.provider:ListProviders",
    # Load
    "load plan": "imcli.commands.load:LoadPlan",
    # Link
    "link": "imcli.commands.link:Link",
    "link concordance list": "imcli.commands.link:LinkConcordanceList",
    "link concordance resolve": "imcli.commands.link:LinkConcordanceResolve",
    "link commit": "imcli.commands.link:LinkCommit",
    # Write
    "write": "imcli.commands.write:Write",
    # Log
    "log tail": "imcli.commands.log:LogTail",
    # Migrate
    "migrate": "imcli.commands.migrate:Migrate",
    }

LOG = logging.getLogger(__name__)


def _find_config() -> Optional[str]:
    possible_paths = (
        os.path.join(os.curdir, ".importmanager.ini"),
        os.path.join(os.path.expanduser("~"), ".importmanager.ini")
        )

    for path in possible_paths:
        if os.path.isfile(path) and os.access(path, os.R_OK):
            return path

    return None


def _load_config(
        f: Optional[Union[str, Iterable[str]]] = None,
        defaults: Dict[str, Dict[str, str]] = dict()) \
        -> configparser.ConfigParser:
    """Loads a configuration from f and uses unset values from defaults for
    filling.

    :param f: Either the path to a config file or an iterable yielding strings
    :param defaults: A dict containing the default values
    :returns: configparser.ConfigParser -- a ConfigParser object
    :raises TypeError: if an invalid f is provided
    """

    config = configparser.ConfigParser(
        interpolation=configparser.ExtendedInterpolation())
    if defaults:
        config.read_dict(defaults)
    if f:
        try:
            if isinstance(f, str):
                config.read(f)
            elif isinstance(f, collections.abc.Iterable):
                config.read_file(f)
            else:
                raise TypeError("f needs to be a path or an open file")
        except configparser.MissingSectionHeaderError:
            raise ConfigError("Could not load config file at '%s'" % f)

    return config


class DictCommandManager(CommandManager):
    """Manages commands and handles lookup based on argv data.

    :param commands: dict containing the mapping from command name to
                     implementing class.
    :param convert_underscores: Whether cliff should convert underscores to
                                spaces in commands.
    """

    def __init__(self, commands: Dict[str, str],
                 convert_underscores: bool = True):
        self.commands = dict()  # type: Dict[str, pkg_resources.EntryPoint]
        self._legacy = dict()  # type: Dict[str, str]
        self.namespace = None
        self.convert_underscores = convert_underscores
        self._load_commands(commands)

    def _load_commands(self, commands: Dict[str, str]) -> None:
        """Process provided commands

        :param commands: dict containing the mapping from command name to
                         implementing class.
        """
        for command, cls_path in commands.items():
            cmd_name = (command.replace("_", " ")
                        if self.convert_underscores else command)
            LOG.debug("Processing command %s", cmd_name)
            try:
                cls_module, cls_name = cls_path.split(":")
                self.commands[cmd_name] = EntryPointWrapper(
                    cmd_name, getattr(
                        importlib.import_module(cls_module), cls_name))
            except (ImportError, AttributeError) as e:
                LOG.exception(e, exc_info=False)
        LOG.debug("Processed commands: %s", self.commands)

    def load_commands(self, namespace: str) -> NoReturn:
        raise AttributeError

    def add_command(self, name: str, command_class: type) -> None:
        self.commands[name] = EntryPointWrapper(name, command_class)

    def add_legacy_command(self, old_name: str, new_name: str) -> NoReturn:
        raise AttributeError


class IMCLI:
    class __IMCLIApp(App):  # noqa: N801
        config = None  # type: configparser.ConfigParser

        def __init__(self) -> None:
            command_manager = DictCommandManager(COMMANDS)

            super().__init__(
                description=__description__, version=__version__,
                command_manager=command_manager)

        def _prepare_config(self) -> None:
            # Load config file
            config_path = self.options.config

            if not (config_path and os.path.isfile(config_path)):
                raise RuntimeError("Could not find config file")

            self.config_path = config_path
            self.config = _load_config(config_path, CONFIG_DEFAULTS)

            self._check_config_sections()

        def _check_config_sections(self) -> None:
            # NOTE: We don't connect to the databases just yet. It might be
            # that the command that's going to be run does not require the
            # database.  The connection will be initiated lazily
            # (which might fail, as only the presence of the config values
            # is checked here not their correctness).

            def _section_require_options(
                    section: str, required_options: Tuple[str, ...]) -> None:
                try:
                    options = dict(self.config.items(section))

                    for opt in required_options:
                        if opt not in options:
                            raise ConfigError(
                                "You must set a %s in the %s section of the "
                                "config file." % (opt, section))
                except configparser.NoSectionError as e:
                    # NOTE: This only happens when the section is not defined
                    # in the defaults.
                    raise ConfigError(
                        "You must set the %s connection parameters in the "
                        "config file." % section) from e

            _section_require_options("state_db", ("url",))
            _section_require_options("sidecar_db", ("url",))
            _section_require_options("histhub_db", ("url", "model_name"))

        def build_option_parser(self, description: str, version: str) \
                -> argparse.ArgumentParser:
            parser = super().build_option_parser(
                description, version)  # type: argparse.ArgumentParser

            parser.add_argument(
                "--config", type=str, nargs="?", dest="config",
                default=_find_config(),
                help="Path to the ImportManager config file")

            return parser

        def configure_logging(self) -> None:
            # NOTE: _prepare_config() is run in configure_logging() instead of
            # initialize_app(), because configure_logging() is called by run()
            # before initilize_app().
            self._prepare_config()

            self.CONSOLE_MESSAGE_FORMAT = self.config.get(
                "importmanager", "logformat", raw=True)

            super().configure_logging()

        def initialize_app(self, remainder: List[str]) -> None:
            self._state_db_conn = None  # type: Optional[StateDBConnection]
            self._sidecar_db_conn = None  # type: Optional[SidecarDBConnection]
            self._histhub_graph = None  # type: Optional[Graph]

        def clean_up(self, cmd: Command, result: int,
                     err: Optional[Exception]) -> None:
            # Close database connections

            # XXX: DRY!
            if self._state_db_conn:
                LOG.debug("Closing state DB connection")
                _tmp1 = self._state_db_conn
                self._state_db_conn = None
                _tmp1.connection.close()
                del _tmp1

            if self._sidecar_db_conn:
                LOG.debug("Closing sidecar DB connection")
                _tmp2 = self._sidecar_db_conn
                self._sidecar_db_conn = None
                _tmp2.connection.close()
                del _tmp2

            if self._histhub_graph:
                LOG.debug("Closing histHub graph connection")
                _tmp3 = self._histhub_graph
                self._histhub_graph = None
                _tmp3.connection.close()
                del _tmp3

        # Custom methods

        def get_state_db_connection(self) -> StateDBConnection:
            """Get a connection object to the state database.

            :raises Exception: whenever something goes wrong (usually the
                               database connection)
            :returns: StateDBConnection - the connection object
            """
            if not self._state_db_conn:
                url = make_url(self.config.get("state_db", "url"))
                LOG.debug("Opening connection to the state DB at %r", url)
                self._state_db_conn = StateDBConnection(url)

            return self._state_db_conn

        def get_sidecar_db_connection(self) -> SidecarDBConnection:
            """Get a connection object to the sidecar database.

            :raises Exception: whenever something goes wrong (usually the
                               database connection)
            :returns: SidecarDBConnection -- the connection object
            """
            if not self._sidecar_db_conn:
                url = make_url(self.config.get("sidecar_db", "url"))
                LOG.debug("Opening connection to the sidecar DB at %r", url)
                self._sidecar_db_conn = SidecarDBConnection(url)

            return self._sidecar_db_conn

        def get_histhub_graph(self) -> Graph:
            """Get a eavlib.graph.Graph to the histHub main graph.

            This method will open a connection if needed

            :raises Exception: whenever something goes wrong (usually the
                               database connection)
            :returns: Graph - the histHub main graph
            """
            if not self._histhub_graph:
                url = make_url(self.config.get("histhub_db", "url"))
                model_name = self.config.get("histhub_db", "model_name")
                LOG.debug(
                    "Opening connection to histHub graph at %r (model: %s)",
                    url, model_name)
                conn = EAVConnection(url)  # type: ignore
                self._histhub_graph = Graph(conn, model_name)

            return self._histhub_graph

    _instance = None  # type: __IMCLIApp

    @classmethod
    def _init(cls) -> None:
        if not cls._instance:
            cls._instance = cls.__IMCLIApp()

    @classmethod
    def instance(cls) -> "__IMCLIApp":
        return cls._instance


def main(argv: List[str] = sys.argv[1:]) -> int:
    IMCLI._init()
    return int(IMCLI.instance().run(argv))
