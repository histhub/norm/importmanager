def confirm(prompt: str, default: bool = False) -> bool:
    choices = "Y/n" if default else "y/N"
    resp = input("%s [%s] " % (prompt, choices)).strip().lower()

    return (resp in ("y", "yes")) if resp else default
