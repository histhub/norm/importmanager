import argparse
import logging

from cliff.command import Command
from cliff.lister import Lister
from normlib import HistHubID

from imcli.main import IMCLI
from imcli.util import confirm

from . import _ListShowRes, positive_int
from importmanager.concordance import Concordance, ConcordanceState
from importmanager.db.concordance_manager import ConcordanceManager
from importmanager.db.job_manager import JobManager
# from importmanager.modules.check import CheckModule
from importmanager.modules.link import (LinkModule, LinkerAlreadyRanError)


LOG = logging.getLogger(__name__)


class Link(Command):
    """Run the histHub linker."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        # parser.add_argument(
        #     "--background", dest="background", action="store_true",
        #     help="Fork the process to the background.")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        state_conn = IMCLI.instance().get_state_db_connection()
        hhb_main = IMCLI.instance().get_histhub_graph()
        job_manager = JobManager(state_conn)  # type: ignore

        arg_job = parsed_args.job_ident
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        # Link data to histHub main
        linker = LinkModule(job, state_conn, hhb_main)

        def exec_(force: bool = False) -> None:
            linker.link(force=force)

        retry, force = False, False
        try:
            exec_()
        except LinkerAlreadyRanError as e:
            if confirm("%s! Overwrite?" % e):
                retry, force = True, True
            else:
                return 1

        if retry:
            exec_(force=force)

        return 0


class LinkConcordanceList(Lister):
    """List concordances of an import job."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        parser.add_argument(
            "--include-resolved", dest="include_resolved", action="store_true",
            help="Pass to list also already resolved concordances")
        parser.add_argument(
            "--limit", type=positive_int, default=25,
            help="The maximum number of results")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore
        concordance_manager = ConcordanceManager(conn)  # type: ignore

        if parsed_args.job_ident.isdigit():
            job = job_manager.get_by_id(int(parsed_args.job_ident))
        else:
            job = job_manager.get_by_name(parsed_args.job_ident)

        concordances = concordance_manager.list_concordances(
            job, include_resolved=parsed_args.include_resolved,
            limit=parsed_args.limit)

        if not concordances:
            LOG.info(
                "No concordances"
                if parsed_args.include_resolved
                else "No unresolved concordances")
            return ((), ())

        cols = ("id", "staging_id", "state", "link_to")
        return (cols,
                tuple(tuple(getattr(j, k) for k in cols)
                      for j in concordances))


class LinkConcordanceResolve(Command):
    """Specify the resolution for a concordance."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        # parser.add_argument(
        #     "-q", "--quiet", dest="quiet", action="store_true",
        #     help="Do not print info messages")
        parser.add_argument(
            "--batch", dest="batch", action="store_true",
            help="Pass this flag when you're calling IMCLI from a shell "
                 "script or similar to suppress interactive prompts")

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")

        parser.add_argument(
            "--concordance", required=True, type=int, dest="concordance_id",
            help="ID of the concordance to resolve")

        parser.add_argument(
            "resolution", metavar="RESOLUTION", nargs="?",
            help="The resolution to the concordance (new or histHub ID to "
                 "link). If omitted an interactive prompt will be shown "
                 "unless --batch is passed, in which case the command will "
                 "fail.")

        return parser

    @classmethod
    def _check_resolution(cls, resol: str) -> bool:
        """Check a given resolution string.

        :param resol: the user given resolution string
        :type resol: str
        :returns: bool -- True if the resolution is valid, False otherwise
        """
        return bool(resol and (resol.isdigit() or resol == "new"))

    @classmethod
    def _store_resolution(cls, concordance: Concordance, resolution: str,
                          concordance_manager: ConcordanceManager) -> None:
        if not cls._check_resolution(resolution):
            raise ValueError("Invalid resolution given")

        if resolution.isdigit():
            # Link to existing histHub entity (value is histHub ID)
            concordance.state = ConcordanceState.linked
            concordance.link_to = HistHubID(resolution)
        elif resolution == "new":
            # Create new histHub entity
            concordance.state = ConcordanceState.new
            concordance.link_to = None
        else:
            raise ValueError("Invalid resolution given")

        concordance_manager.store_concordance(concordance)

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore
        concordance_manager = ConcordanceManager(conn)  # type: ignore

        if parsed_args.job_ident.isdigit():
            job = job_manager.get_by_id(int(parsed_args.job_ident))
        else:
            job = job_manager.get_by_name(parsed_args.job_ident)

        concordance = concordance_manager.fetch_concordance(
            job, int(parsed_args.concordance_id))

        resolution = parsed_args.resolution

        if not resolution and not parsed_args.batch:
            print("Possible candidates:")
            if concordance.candidates:
                pass
            else:
                print("  None")
            print()

            # Ask for resolution
            while not self.__class__._check_resolution(resolution):
                print("Give a resolution ('new' or histHub ID to link):")
                resolution = input("--> ")

        if resolution:
            self.__class__._store_resolution(
                concordance, resolution, concordance_manager)
            return 0

        # No resolution (e.g. interactivity required but --batch is set)
        return 2


class LinkCommit(Command):
    """Finish the linking step. This step makes concordances immutable."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        # parser.add_argument(
        #     "--background", dest="background", action="store_true",
        #     help="Fork the process to the background.")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        state_conn = IMCLI.instance().get_state_db_connection()
        hhb_main = IMCLI.instance().get_histhub_graph()
        job_manager = JobManager(state_conn)  # type: ignore

        arg_job = parsed_args.job_ident
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        LinkModule(job, state_conn, hhb_main).commit()

        # Run label checker
        # CheckModule(job, state_conn).check_labels()

        return 0
