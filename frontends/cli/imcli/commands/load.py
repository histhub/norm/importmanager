import argparse
import errno
import logging
import os

from cliff.command import Command

from importmanager.db.job_manager import JobManager
from importmanager.modules.load import LoadModule

from imcli.main import IMCLI
from imcli.errors import ConfigError

LOG = logging.getLogger(__name__)


class LoadPlan(Command):
    """Load the load plan for an import job and import the referenced data."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        parser.add_argument(
            "path", metavar="PATH", nargs=1, type=str,
            help="Path to the load plan file")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        state_conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(state_conn)  # type: ignore

        load_plan = parsed_args.path[0]

        model_file = os.path.realpath(os.path.join(
            os.path.dirname(IMCLI.instance().config_path),
            IMCLI.instance().config.get("importmanager", "model")))
        if not os.path.isfile(model_file):
            raise ConfigError("Cannot find EAV model description: %s: %s" % (
                model_file, os.strerror(errno.ENOENT)))
        LOG.info("Using model description file: %s", model_file)

        arg_job = parsed_args.job_ident
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        # Load data into temp. schema
        loader = LoadModule(job, state_conn)
        loader.load_from_plan(load_plan, model_file)  # TODO: Pass verbose

        return 0
