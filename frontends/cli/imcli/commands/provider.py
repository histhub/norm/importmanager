import argparse
import logging

from cliff.lister import Lister
from cliff.show import ShowOne

from . import _ListShowRes
from imcli.main import IMCLI
from importmanager.db.provider_manager import ProviderManager

LOG = logging.getLogger(__name__)


class CreateProvider(ShowOne):
    """Create a new import provider."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("name", metavar="NAME", nargs=1)

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        provider_manager = ProviderManager(conn)  # type: ignore

        provider = provider_manager.create_provider(parsed_args.name[0])

        return tuple(zip(*provider))  # type: ignore


class ListProviders(Lister):
    """List import providers."""

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        provider_manager = ProviderManager(conn)  # type: ignore

        providers = provider_manager.list_providers()

        if providers:
            return (tuple(dict(providers[0])),
                    tuple((tuple(dict(p).values()) for p in providers)))
        return ((), ())
