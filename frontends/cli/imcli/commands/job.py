import argparse
import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from imcli.main import IMCLI
from importmanager.db.job_manager import JobManager
from importmanager.db.provider_manager import ProviderManager
from importmanager.job import JobState
from . import _ListShowRes

LOG = logging.getLogger(__name__)


class CreateImport(ShowOne):
    """Create a new import job."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("--provider", required=True)
        parser.add_argument("name", metavar="NAME", nargs=1)

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore
        provider_manager = ProviderManager(conn)  # type: ignore

        arg_provider = parsed_args.provider
        if arg_provider.isdigit():
            import_provider = provider_manager.get_by_id(int(arg_provider))
        else:
            import_provider = provider_manager.get_by_name(arg_provider)

        import_name = parsed_args.name[0]
        job = job_manager.create_job(import_name, import_provider)

        return tuple(zip(*job))  # type: ignore


class ListImports(Lister):
    """List import jobs."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--include", nargs="+", metavar="STATE", default=[])
        parser.add_argument(
            "--exclude", nargs="+", metavar="STATE", default=[])

        parser.add_argument("--all", action="store_true")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore

        if not parsed_args.all:
            exclude_set = {JobState.aborted, JobState.finished}
            for _ in parsed_args.exclude:
                exclude_set.add(JobState(_))
            for _ in parsed_args.include:
                exclude_set.discard(JobState(_))
        else:
            if parsed_args.exclude:
                raise ValueError("--exclude is incompatible with --all!")
            if parsed_args.include:
                LOG.warning("--include is meaningless with --all")
            exclude_set = set()

        jobs = sorted(
            job_manager.list_jobs(exclude_states=tuple(exclude_set)),
            key=lambda job: job.last_action or job.created_at)

        cols = ("id", "name", "provider", "state", "step", "last_action")
        return (cols, tuple(tuple(getattr(j, k) for k in cols) for j in jobs))


class ShowImport(ShowOne):
    """Show details of an import job."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        parser.add_argument(
            "ident", metavar="IDENT", nargs=1,
            help="Identifier (ID or name) of the import job to show")
        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> _ListShowRes:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore

        arg_job = parsed_args.ident[0]
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        return tuple(zip(*job))  # type: ignore


class CancelImport(Command):
    """Cancel a running import."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        parser.add_argument(
            "ident", metavar="IDENT", nargs=1,
            help="Identifier (ID or name) of the import job to cancel")
        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore

        arg_job = parsed_args.ident[0]
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        try:
            job_manager.cancel_job(job)
            LOG.info("Import job %s has been cancelled", job)
        except Exception:
            LOG.error("An error occurred when cancelling the job %s:", job)
            raise

        return 0
