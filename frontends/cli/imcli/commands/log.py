import argparse
import logging

from cliff.command import Command

from . import positive_int
from imcli.main import IMCLI
from importmanager.db.job_manager import JobManager
from importmanager.db.log_manager import LogManager

LOG = logging.getLogger(__name__)


class LogTail(Command):
    """Get the tail of the logs for an import job."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        parser.add_argument(
            "-l", "--level", required=False, default=str(logging.INFO),
            dest="loglevel",
            help="The minimum required log level to be printed")
        parser.add_argument(
            "--format", required=False, type=str, dest="fmt",
            default="%(levelname)-8s %(asctime)s %(message)s",
            help="The format string specifies the way log messages get logged")

        parser.add_argument(
            "-n", "--limit", metavar="LIMIT", dest="limit", type=positive_int,
            default=100, help="Maximum number of records to print")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        conn = IMCLI.instance().get_state_db_connection()
        job_manager = JobManager(conn)  # type: ignore
        log_manager = LogManager(conn)  # type: ignore

        formatter = logging.Formatter(fmt=parsed_args.fmt)

        arg_job = parsed_args.job_ident
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        if parsed_args.loglevel.isdigit():
            loglevel = int(parsed_args.loglevel)
        else:
            loglevel = getattr(logging, parsed_args.loglevel.upper(), None)
            if not isinstance(loglevel, int):
                raise ValueError(
                    "Invalid log level: %s" % parsed_args.loglevel)

        records = log_manager.tail_logs(job, loglevel, parsed_args.limit)

        for r in records:
            print(formatter.format(r))

        return 0
