import argparse

from cliff.command import Command
from eavlib.db.url import make_url
from typing import TYPE_CHECKING

from importmanager.db.migrator import SchemaMigrator
from importmanager.db.state_db import StateDBConnection

from imcli.main import IMCLI

if TYPE_CHECKING:
    from cliff.app import App
    from typing import (Any, Optional)


def confirm(prompt: str, default: bool = False) -> bool:
    choices = "Y/n" if default else "y/N"
    resp = input("%s [%s] " % (prompt, choices)).strip().lower()

    return (resp in ("y", "yes")) if resp else default


def positive_int(num: str) -> int:
    i = int(num)
    if i < 0:
        raise argparse.ArgumentTypeError(
            "%r is not a positive integer" % num)
    return i


class Migrate(Command):
    """Run database migrations on the ImportManager database."""

    def __init__(self, app: "App", app_args: "Any",
                 cmd_name: "Optional[str]" = None) -> None:
        super().__init__(app, app_args, cmd_name)

        import curses

        curses.setupterm()
        self._has_colour_term = curses.tigetnum("colors") > 1

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "connection", help="database connection URL",
            nargs="?", default=None)
        parser.add_argument(
            "--migrate", required=False, type=positive_int, metavar="VERSION",
            nargs="?", const=SchemaMigrator._latest_version,
            help="Migrate schema to a newer version. "
            "The optional argument specifies the version to migrate to.")
        parser.add_argument(
            "--debug", required=False, action="store_true",
            help="Print messages useful for debugging.")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        if parsed_args.connection:
            state_db = StateDBConnection(make_url(parsed_args.connection))
        else:
            state_db = IMCLI.instance().get_state_db_connection()

        migrator = SchemaMigrator(  # type: ignore
            state_db, debug=parsed_args.debug)

        if not migrator.is_created():
            print("State DB has not been initialised, yet.")

            # NOTE: Skip the schema logic because the default schema should
            #       always be present.
            assert migrator.schema_exists()

            print("Initialising state DB...")
            migrator.create()

            if parsed_args.migrate is None:
                # By default: migrate to latest version after schema is inited.
                parsed_args.migrate = SchemaMigrator._latest_version

        if parsed_args.migrate is not None:
            target_version = parsed_args.migrate
            if migrator.migration_needed(target_version):
                print("Migrating state DB to version %u..." % target_version)
                migrator.migrate(target_version)
            else:
                print("State DB is already at version %u. Nothing to do." % (
                    migrator.current_version()))
        else:
            print("Nothing to do.")

        return 0
