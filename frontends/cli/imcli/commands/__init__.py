import argparse

from typing import (Any, Tuple)

_ListShowRes = Tuple[Tuple[str, ...], Tuple[Any, ...]]


def positive_int(value: Any) -> int:
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError(
            "%s is an invalid positive int value" % value)
    return ivalue
