import argparse

from cliff.command import Command
from normlib.connection import NormDBConnection

from importmanager.db.job_manager import JobManager
from importmanager.modules.write import WriteModule

from imcli.main import IMCLI


class Write(Command):
    """Write a job to the main histHub database."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "--import", required=True, dest="job_ident",
            help="Identifier (ID or name) of the import job")
        # parser.add_argument(
        #     "--background", dest="background", action="store_true",
        #     help="Fork the process to the background.")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        state_conn = IMCLI.instance().get_state_db_connection()
        sidecar_conn = IMCLI.instance().get_sidecar_db_connection()
        hhb_main = IMCLI.instance().get_histhub_graph()
        job_manager = JobManager(state_conn)  # type: ignore

        arg_job = parsed_args.job_ident
        if arg_job.isdigit():
            job = job_manager.get_by_id(int(arg_job))
        else:
            job = job_manager.get_by_name(arg_job)

        # Run writer
        norm_db = NormDBConnection(hhb_main, sidecar_conn)
        WriteModule(job, state_conn, norm_db).write()

        return 0
