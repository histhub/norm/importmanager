#!/usr/bin/env python3

"""
Entry point for ImportManager CLI.
Execute with
$ python3 -m imcli
"""

from imcli.main import main


if __name__ == "__main__":
    import os
    import sys

    if __package__ is None and not hasattr(sys, "frozen"):
        # Direct call of __main__.py
        _path = os.path.realpath(os.path.abspath(__file__))
        sys.path.insert(0, os.path.dirname(os.path.dirname(_path)))

    status = None
    try:
        argv = sys.argv[1:]
        status = main(argv)
    except KeyboardInterrupt:
        pass

    sys.exit(status)
