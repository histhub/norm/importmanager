import datetime
import functools

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import (Any, Callable, List, Optional, Sequence, TypeVar)
    X = TypeVar("X")
    Y = TypeVar("Y")


def splat(func: "Callable[[Sequence[X]], Y]") -> "Callable[..., Y]":
    @functools.wraps(func)
    def wrapper(*args: "X") -> "Y":
        return func(args)
    return wrapper


def unsplat(func: "Callable[..., Y]") -> "Callable[[Sequence[X]], Y]":
    @functools.wraps(func)
    def wrapper(seq: "Sequence[X]") -> "Y":
        return func(*seq)
    return wrapper


class AutoFlushCache:
    # ATTENTION: This class is not thread-safe

    def __init__(self, flush_func: "Callable[[List[Any]], None]",
                 max_len: "Optional[int]",
                 max_age: "Optional[datetime.timedelta]") -> None:
        """Creates a new AutoFlushCache.

        NOTE: Make sure to always manually flush() the cache before disposing
        it.

        :param flush_func: a callback to be called when the cache flushes.
        :type flush_func: Callable[[List[Any]], None]
        :param max_len: maximum number of items in the cache before it is
        flushed.
        :type max_len: Optional[int]
        :param max_age: maximum age of oldest item in cache before it is
        flushed.
        NOTE: The age is only checked when a new item is added to the cache.
        :type max_age: Optional[datetime.timedelta]
        """
        self.max_len = max_len
        self.max_age = max_age
        self._flush = flush_func

        self._reset()

    def _reset(self) -> None:
        self._items = []  # type: List[Any]
        self._last_flush = datetime.datetime.now()

    def _autoflush(self) -> None:
        if self.max_len and len(self._items) >= self.max_len:
            self.flush()
            return

        if self.max_age and (
                datetime.datetime.now() - self._last_flush) >= self.max_age:
            self.flush()

    def add(self, item: "Any") -> None:
        """Appends a new item to the cache."""
        self._items.append(item)
        self._autoflush()

    def flush(self) -> None:
        """Calls the flush_func immediately with all items currently in the
        cache.
        """
        items = self._items
        self._reset()
        self._flush(items)
