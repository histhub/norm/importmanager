from typing import Any, Iterator, Optional, Tuple


class Provider:
    def __init__(self, name: str, id: Optional[int] = None) -> None:
        self._id = id
        self._name = name

    def __eq__(self, other: object) -> bool:
        if type(self) == type(other):
            return vars(self) == vars(other)

        return NotImplemented

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        yield ("id", self.id)
        yield ("name", self.name)

    def __str__(self) -> str:
        return self.name

    @property
    def id(self) -> Optional[int]:
        return self._id

    @property
    def name(self) -> str:
        return self._name
