# ImportManager metadata

__package_name__ = "importmanager"
__description__ = ("A library to import data from external sources into the "
                   "histHub database")

__author__ = "SSRQ-SDS-FDS Law Sources Foundation of the Swiss Lawyers Society"
__copyright__ = ("Copyright 2019-2021 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (1, 0)
__version__ = "1.0"
