import enum

from typing import TYPE_CHECKING

from importmanager.job import Job

if TYPE_CHECKING:
    from normlib import HistHubID
    from typing import (Any, Dict, Iterator, NewType, Optional, Tuple)
    Percentage = NewType("Percentage", float)


@enum.unique
class ConcordanceState(enum.Enum):
    undecided = "undecided"
    linked = "linked"
    new = "new"

    def __str__(self) -> str:
        return str(self.value)


class Concordance:
    __slots__ = ("id", "job", "staging_id", "candidates", "state", "link_to",
                 "dirty")

    def __init__(self,
                 job: Job,
                 staging_id: int,
                 candidates: "Dict[HistHubID, Percentage]",
                 state: ConcordanceState,
                 link_to: "Optional[HistHubID]",
                 dirty: bool = False,
                 id: "Optional[int]" = None) -> None:
        self.id = id
        self.job = job
        self.staging_id = staging_id
        self.candidates = candidates
        self.state = state
        self.link_to = link_to
        self.dirty = dirty

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return iter(self) == iter(other)
        return False

    def __iter__(self) -> "Iterator[Tuple[str, Any]]":
        for attr in self.__slots__:
            yield (attr, getattr(self, attr))
