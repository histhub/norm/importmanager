--
-- PostgreSQL migration for ImportManager
-- 0004: Update job_step enum
--

--
-- Name: job_step; Type: TYPE
--

ALTER TYPE "{SCHEMA_NAME}".job_step
    RENAME VALUE 'check_before_link' TO 'check';
ALTER TYPE "{SCHEMA_NAME}".job_step
    RENAME VALUE 'check_after_link' TO 'check_labels';
