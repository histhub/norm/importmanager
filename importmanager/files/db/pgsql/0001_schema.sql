--
-- PostgreSQL database for ImportManager
--

--
-- Name: job_state; Type: TYPE
--

CREATE TYPE "{SCHEMA_NAME}".job_state AS ENUM (
    'running',
    'crashed',
    'active',
    'finished',
    'aborted'
);


--
-- Name: job_step; Type: TYPE
--

CREATE TYPE "{SCHEMA_NAME}".job_step AS ENUM (
    'none',
    'load',
    'check_before_link',
    'link',
    'check_after_link',
    'write',
    'completed'
);


--
-- Name: attribute; Type: TABLE
--

CREATE TABLE "{SCHEMA_NAME}".attribute (
    id integer NOT NULL,
    import_id integer,
    state "{SCHEMA_NAME}".job_state,
    key character varying(255),
    value text
);


--
-- Name: attribute_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE "{SCHEMA_NAME}".attribute_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attribute_id_seq; Type: SEQUENCE OWNED BY
--

ALTER SEQUENCE "{SCHEMA_NAME}".attribute_id_seq
    OWNED BY "{SCHEMA_NAME}".attribute.id;


--
-- Name: concordance; Type: TABLE
--

CREATE TABLE "{SCHEMA_NAME}".concordance (
    id integer NOT NULL,
    import_id integer NOT NULL,
    input_id integer,
    candidate_id integer,
    similarity double precision
);


--
-- Name: COLUMN concordance.input_id; Type: COMMENT
--

COMMENT ON COLUMN "{SCHEMA_NAME}".concordance.input_id IS
    'ID of the element in the input data (=data to be imported)';


--
-- Name: COLUMN concordance.candidate_id; Type: COMMENT
--

COMMENT ON COLUMN "{SCHEMA_NAME}".concordance.candidate_id IS
    'ID of the candidate element in the histHub database';


--
-- Name: concordance_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE "{SCHEMA_NAME}".concordance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: concordance_id_seq; Type: SEQUENCE OWNED BY
--

ALTER SEQUENCE "{SCHEMA_NAME}".concordance_id_seq
    OWNED BY "{SCHEMA_NAME}".concordance.id;


--
-- Name: concordance_import_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE "{SCHEMA_NAME}".concordance_import_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: concordance_import_id_seq; Type: SEQUENCE OWNED BY
--

ALTER SEQUENCE "{SCHEMA_NAME}".concordance_import_id_seq OWNED BY "{SCHEMA_NAME}".concordance.import_id;


--
-- Name: job; Type: TABLE
--

CREATE TABLE "{SCHEMA_NAME}".job (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    provider_id integer,
    state public.job_state DEFAULT 'active'::public.job_state,
    step public.job_step DEFAULT 'none'::public.job_step,
    temp_schema character varying(255) UNIQUE
);


--
-- Name: job_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE "{SCHEMA_NAME}".job_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_id_seq; Type: SEQUENCE OWNED BY
--

ALTER SEQUENCE "{SCHEMA_NAME}".job_id_seq OWNED BY "{SCHEMA_NAME}".job.id;


--
-- Name: provider; Type: TABLE
--

CREATE TABLE "{SCHEMA_NAME}".provider (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: provider_id_seq; Type: SEQUENCE; Schema: public
--

CREATE SEQUENCE public.provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: provider_id_seq; Type: SEQUENCE OWNED BY
--

ALTER SEQUENCE "{SCHEMA_NAME}".provider_id_seq
    OWNED BY "{SCHEMA_NAME}".provider.id;


--
-- Name: attribute id; Type: DEFAULT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".attribute
    ALTER COLUMN id SET DEFAULT nextval(
        '"{SCHEMA_NAME}".attribute_id_seq'::regclass);


--
-- Name: concordance id; Type: DEFAULT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".concordance
    ALTER COLUMN id SET DEFAULT nextval(
        '"{SCHEMA_NAME}".concordance_id_seq'::regclass);


--
-- Name: concordance import_id; Type: DEFAULT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".concordance
    ALTER COLUMN import_id SET DEFAULT nextval(
        '"{SCHEMA_NAME}".concordance_import_id_seq'::regclass);


--
-- Name: job id; Type: DEFAULT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".job
    ALTER COLUMN id SET DEFAULT nextval(
        '"{SCHEMA_NAME}".job_id_seq'::regclass);


--
-- Name: provider id; Type: DEFAULT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".provider
    ALTER COLUMN id SET DEFAULT nextval(
        '"{SCHEMA_NAME}".provider_id_seq'::regclass);


--
-- Name: attribute_id_seq; Type: SEQUENCE SET
--

SELECT pg_catalog.setval('"{SCHEMA_NAME}".attribute_id_seq', 1, false);


--
-- Name: concordance_id_seq; Type: SEQUENCE SET
--

SELECT pg_catalog.setval('"{SCHEMA_NAME}".concordance_id_seq', 1, false);


--
-- Name: concordance_import_id_seq; Type: SEQUENCE SET
--

SELECT pg_catalog.setval('"{SCHEMA_NAME}".concordance_import_id_seq', 1, false);


--
-- Name: job_id_seq; Type: SEQUENCE SET
--

SELECT pg_catalog.setval('"{SCHEMA_NAME}".job_id_seq', 1, false);


--
-- Name: provider_id_seq; Type: SEQUENCE SET
--

SELECT pg_catalog.setval('"{SCHEMA_NAME}".provider_id_seq', 1, false);


--
-- Name: attribute attribute_pkey; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".attribute
    ADD CONSTRAINT attribute_pkey PRIMARY KEY (id);


--
-- Name: concordance concordance_pkey; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".concordance
    ADD CONSTRAINT concordance_pkey PRIMARY KEY (id);


--
-- Name: job job_name_key; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".job
    ADD CONSTRAINT job_name_key UNIQUE (name);


--
-- Name: job job_pkey; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: provider provider_name_key; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".provider
    ADD CONSTRAINT provider_name_key UNIQUE (name);


--
-- Name: provider provider_pkey; Type: CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".provider
    ADD CONSTRAINT provider_pkey PRIMARY KEY (id);


--
-- Name: attribute_import_id_state_key_idx; Type: INDEX
--

CREATE UNIQUE INDEX attribute_import_id_state_key_idx
    ON "{SCHEMA_NAME}".attribute
    USING btree
    (import_id, state, key);


--
-- Name: attribute attribute_import_id_fkey; Type: FK CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".attribute
    ADD CONSTRAINT attribute_import_id_fkey FOREIGN KEY (import_id)
        REFERENCES "{SCHEMA_NAME}".job(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: concordance concordance_import_id_fkey; Type: FK CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".concordance
    ADD CONSTRAINT concordance_import_id_fkey FOREIGN KEY (import_id)
        REFERENCES "{SCHEMA_NAME}".job(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: job job_provider_fkey; Type: FK CONSTRAINT
--

ALTER TABLE ONLY "{SCHEMA_NAME}".job
    ADD CONSTRAINT job_provider_fkey FOREIGN KEY (provider_id)
        REFERENCES "{SCHEMA_NAME}".provider(id)
        ON UPDATE CASCADE ON DELETE RESTRICT;
