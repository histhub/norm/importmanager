--
-- PostgreSQL migration for ImportManager
-- 0008: Add model_id column to job
--

--
-- Name: job; Type: TABLE
--

-- Add model_id column to job
ALTER TABLE "{SCHEMA_NAME}".job ADD COLUMN IF NOT EXISTS model_id integer;
