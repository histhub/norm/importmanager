--
-- PostgreSQL migration for ImportManager
-- 0002: Logs
--

--
-- Name: log; Type: TABLE
--

CREATE TABLE "{SCHEMA_NAME}".log (
    id SERIAL PRIMARY KEY,
    import_id integer NOT NULL REFERENCES "{SCHEMA_NAME}".job(id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    timestamp timestamp(4) with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    logger character varying(255),
    level smallint NOT NULL,
    message text,
    trace text
);
