--
-- PostgreSQL migration for ImportManager
-- 0002: Logs (extended with file and line information)
--

--
-- Name: log; Type: TABLE
--

ALTER TABLE "{SCHEMA_NAME}".log
    ADD COLUMN IF NOT EXISTS pathname varchar,
    ADD COLUMN IF NOT EXISTS lineno integer;

UPDATE "{SCHEMA_NAME}".log SET pathname = '' WHERE pathname IS NULL;
UPDATE "{SCHEMA_NAME}".log SET lineno = 0 WHERE lineno IS NULL;

ALTER TABLE "{SCHEMA_NAME}".log
    ALTER COLUMN pathname SET NOT NULL,
    ALTER COLUMN lineno SET NOT NULL;
