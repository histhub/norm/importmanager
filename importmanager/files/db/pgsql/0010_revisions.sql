--
-- PostgreSQL migration for ImportManager
-- 0010: Add support for revisions
--

--
-- Name: job; Type: TABLE
--

-- Add revision_id column to job

-- NOTE: It must be UNIQUE, as all imports should be written atomically, thus
-- one import is equal to one revision.
ALTER TABLE "{SCHEMA_NAME}".job
    ADD COLUMN IF NOT EXISTS revision_id integer NULL UNIQUE;
