--
-- PostgreSQL migration for ImportManager
-- 0009: Auto update last_action column in job table
--

--
-- Name: job; Type: TABLE
--

-- Auto update last_action column when a row in the job table is updated
CREATE OR REPLACE FUNCTION "{SCHEMA_NAME}".update_last_action()
RETURNS TRIGGER AS $$
BEGIN
  NEW.last_action = now();
  RETURN NEW;
END;
$$ language 'plpgsql';

DROP TRIGGER IF EXISTS update_last_action ON "{SCHEMA_NAME}".job;

CREATE TRIGGER update_last_action
    BEFORE UPDATE ON "{SCHEMA_NAME}".job
    FOR EACH ROW EXECUTE PROCEDURE "{SCHEMA_NAME}".update_last_action();
