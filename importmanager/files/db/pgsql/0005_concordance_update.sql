--
-- PostgreSQL migration for ImportManager
-- 0005: Update concordance table
--

--
-- Name: concordance; Type: TABLE
--

ALTER TABLE "{SCHEMA_NAME}".concordance RENAME COLUMN input_id TO staging_id;
ALTER TABLE "{SCHEMA_NAME}".concordance ALTER COLUMN staging_id SET NOT NULL;
COMMENT ON COLUMN "{SCHEMA_NAME}".concordance.staging_id IS
    'Row ID of the element in the staging schema';

ALTER TABLE "{SCHEMA_NAME}".concordance DROP COLUMN IF EXISTS candidate_id;
ALTER TABLE "{SCHEMA_NAME}".concordance DROP COLUMN IF EXISTS similarity;

ALTER TABLE "{SCHEMA_NAME}".concordance ADD COLUMN IF NOT EXISTS candidates jsonb;
COMMENT ON COLUMN "{SCHEMA_NAME}".concordance.candidates IS
    'A JSON object that describes the possible candidates for staging_id of the form: {{histhub_id: similarity, ...}}';
ALTER TABLE "{SCHEMA_NAME}".concordance ADD COLUMN IF NOT EXISTS link_to integer;
COMMENT ON COLUMN "{SCHEMA_NAME}".concordance.link_to IS
    'histHub ID to link this entity to (NULL means undecided, -1 means new entity)';
