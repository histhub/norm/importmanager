--
-- PostgreSQL migration for ImportManager
-- 0007: Add concordance state column
--

--
-- Name: concordance_state; Type: TYPE
--

CREATE TYPE "{SCHEMA_NAME}".concordance_state AS ENUM (
  'undecided',
  'linked',
  'new',
  'error'
);


--
-- Name: concordance; Type: TABLE
--
-- Add state column

ALTER TABLE "{SCHEMA_NAME}".concordance
    ADD COLUMN IF NOT EXISTS state concordance_state DEFAULT 'undecided';

-- Update link_to column
UPDATE "{SCHEMA_NAME}".concordance SET state = 'error';
UPDATE "{SCHEMA_NAME}".concordance SET state = 'undecided'
    WHERE link_to IS NULL;
UPDATE "{SCHEMA_NAME}".concordance SET state = 'new', link_to = NULL
    WHERE link_to = -1;
UPDATE "{SCHEMA_NAME}".concordance SET state = 'linked' WHERE link_to > 0;
