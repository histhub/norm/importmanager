--
-- PostgreSQL migration for ImportManager
-- 0003: Logs
--

--
-- Name: job; Type: TABLE
--

ALTER TABLE "{SCHEMA_NAME}".job
    ADD COLUMN IF NOT EXISTS created_at timestamp(0) with time zone NOT NULL
        DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE "{SCHEMA_NAME}".job
    ADD COLUMN IF NOT EXISTS last_action timestamp(0) with time zone;
ALTER TABLE "{SCHEMA_NAME}".job ADD COLUMN IF NOT EXISTS error text;
