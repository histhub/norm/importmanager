--
-- PostgreSQL migration for ImportManager
-- 0006: Add next_step column to job table
--

--
-- Name: job; Type: TABLE
--

ALTER TABLE "{SCHEMA_NAME}".job ALTER COLUMN step SET DEFAULT 'none';
ALTER TABLE "{SCHEMA_NAME}".job
    ADD COLUMN IF NOT EXISTS next_step job_step DEFAULT 'load';
UPDATE "{SCHEMA_NAME}".job SET next_step = step
    WHERE next_step = 'load' AND step != 'none';
UPDATE "{SCHEMA_NAME}".job SET next_step = 'none' WHERE step = 'completed';
