import enum

from datetime import datetime
from types import TracebackType
from typing import Any, Iterator, Optional, Tuple, Type, TYPE_CHECKING
from eavlib.connection import EAVConnection

from importmanager.provider import Provider
from importmanager.db.state_db import StateDBConnection

if TYPE_CHECKING:
    from importmanager.db.job_manager import JobManager


class RunningJob:
    def __init__(self, job: "Job", job_manager: "JobManager",
                 step: "JobStep", singleton: bool = False) -> None:
        self.job = job
        self.job_manager = job_manager
        self.step = step
        self.singleton = singleton

    def set_next_step(self, next_step: "JobStep") -> None:
        self.job.next_step = next_step

    def __enter__(self) -> "RunningJob":
        if self.job.id:
            db_job = self.job_manager.get_by_id(self.job.id)
            if db_job and db_job.state is JobState.running:
                raise RuntimeError("Job %r is already running" % self.job)

        if self.singleton:
            running_jobs = list(filter(
                lambda job: job.state is JobState.running,
                self.job_manager.list_jobs(
                    exclude_states=(JobState.finished, JobState.aborted))))
            if running_jobs:
                raise RuntimeError(
                    "There are currently %u running jobs, but the current "
                    "operation requires to be the only running operation!" % (
                        len(running_jobs)))

            if not self.job.id:
                # Notify the user that singletons do not realiably work when
                # the jobs are not in the database, as there could be two jobs
                # concurrently running which are not in the DB.
                import logging
                logging.getLogger(__name__).warning(
                    "Singleton jobs do not work realiably without an ID!")

        self.job.state = JobState.running
        self.job.step = self.step
        if self.job.id:
            self.job_manager.store_job(self.job)

        return self

    def __exit__(self,
                 et: Optional[Type[BaseException]],
                 ev: Optional[BaseException],
                 tb: Optional[TracebackType]) -> None:
        if et and ev:
            # An exception occurred during the job dun
            self.job.state = JobState.crashed
            self.job.error = "%s: %s" % (et.__name__, ev)
        else:
            # Successful jo run
            self.job.state = JobState.active
            self.job.error = None

        if self.job.id:
            # NOTE: We commit the last transaction so that we can
            # always update the job, even if a query in the
            # transaction failed.
            self.job_manager.db.commit()

            self.job_manager.store_job(self.job)

        # implicit return of None => don't swallow exceptions


@enum.unique
class JobState(enum.Enum):
    running = "running"
    crashed = "crashed"
    active = "active"
    finished = "finished"
    aborted = "aborted"

    def __str__(self) -> str:
        return str(self.value)


@enum.unique
class JobStep(enum.Enum):
    none = "none"
    load = "load"
    check = "check"
    link = "link"
    check_labels = "check_labels"
    write = "write"
    completed = "completed"

    def __str__(self) -> str:
        return str(self.value)


class Job:
    __slots__ = ("id", "name", "provider", "state", "step", "next_step",
                 "created_at", "last_action", "temp_schema", "model_id",
                 "revision_id", "error")

    def __init__(self, name: str,
                 provider: Provider,
                 id: int,
                 state: JobState = JobState.active,
                 step: JobStep = JobStep.none,
                 next_step: JobStep = JobStep.load,
                 created_at: datetime = datetime.now(),
                 last_action: Optional[datetime] = None,
                 temp_schema: Optional[str] = None,
                 model_id: Optional[int] = None,
                 revision_id: Optional[int] = None,
                 error: Optional[str] = None) -> None:

        if not Job._is_valid_name(name):
            raise ValueError("Name '%s' is not permissible" % name)

        self.id = id
        self.name = name
        self.provider = provider
        self.state = state
        self.step = step
        self.next_step = next_step
        self.created_at = created_at
        self.last_action = last_action
        self.temp_schema = temp_schema
        self.model_id = model_id
        self.revision_id = revision_id
        self.error = error

    @classmethod
    def _is_valid_name(cls, name: str) -> bool:
        """Checks if `name` is a permissible Job name.

        Names must be non-empty and must not start with a digit.

        :param name: the name to check
        :type name: str
        :returns: bool -- True if the name is permissible
        """
        return bool(name and not name[0].isdigit())

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return iter(self) == iter(other)
        return False

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        for attr in self.__slots__:
            yield (attr, getattr(self, attr))

    def __str__(self) -> str:
        return self.name

    def eav_connection(self, state_db: StateDBConnection) -> EAVConnection:
        if not self.temp_schema:
            raise ValueError(
                "This job has no temp. schema assigned. "
                "Cannot open an EAVConnection.")
        return EAVConnection.fromconnection(
            state_db.connection, self.temp_schema)

    def run(self, job_manager: "JobManager", step: JobStep,
            singleton: bool = False) -> "RunningJob":
        return RunningJob(self, job_manager, step, singleton)
