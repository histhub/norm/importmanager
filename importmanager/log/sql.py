import logging

from typing import Any, Union

from importmanager.db.log_manager import LogManager
from importmanager.job import Job


class SQLLogHandler(logging.Handler):
    def __init__(self, db: Any, job: Job,
                 level: Union[int, str] = logging.NOTSET) -> None:
        super().__init__(level)
        self.db = db
        self.job = job
        self.log_manager = LogManager(self.db)  # type: ignore

    def emit(self, record: logging.LogRecord) -> None:
        self.log_manager.log(self.job, record)
