import contextlib
import logging

from types import TracebackType
from typing import (Optional, Tuple, Type, TYPE_CHECKING, Union)

if TYPE_CHECKING:
    ExcInfoType = Tuple[Type[BaseException], BaseException,
                        Optional[TracebackType]]


# This class has been copied from:
# https://docs.python.org/3.6/howto/logging-cookbook.html

class LoggingContext(contextlib.ContextDecorator):
    def __init__(self, logger: logging.Logger,
                 level: Optional[Union[int, str]] = None,
                 handler: Optional[logging.Handler] = None,
                 close: bool = True) -> None:
        self.logger = logger
        self.level = level
        self.handler = handler
        self.close = close

    def __enter__(self) -> None:
        if self.level is not None:
            self.old_level = self.logger.level
            self.logger.setLevel(self.level)
        if self.handler:
            self.logger.addHandler(self.handler)

    def __exit__(self, et: Optional[Type[BaseException]],
                 ev: Optional[BaseException],
                 tb: Optional[TracebackType]) -> None:
        if et and ev:
            exc_info = (et, ev, tb)  # type: ExcInfoType
            self.logger.exception(
                "%s: %s" % (et.__name__, ev), exc_info=exc_info)

        if self.level is not None:
            self.logger.setLevel(self.old_level)
        if self.handler:
            self.logger.removeHandler(self.handler)
        if self.handler and self.close:
            self.handler.close()

        # implicit return of None => don't swallow exceptions
