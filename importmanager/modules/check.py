import logging

from importmanager.job import (Job, JobState, JobStep)
from importmanager.db.job_manager import JobManager
from importmanager.db.state_db import StateDBConnection
from importmanager.log import LoggingContext
from importmanager.log.sql import SQLLogHandler

LOG = logging.getLogger(__name__)


class CheckModule:
    def __init__(self, job: Job, state_db: StateDBConnection) -> None:
        """Initialise the CheckModule.

        :param job: the import job for which data consistency should be checked
        :param db: an open database connection to the state schema
        """
        if job.state in (JobState.finished, JobState.aborted):
            raise ValueError("Job %r is %s. No further steps are possible." % (
                job, job.state))

        if job.next_step is not JobStep.check_labels:
            raise ValueError(
                "Job's next_step must be in check_labels step. "
                "The job %r has next_step '%s'." % (job, job.next_step))

        self.job = job
        self.state_db = state_db

        self.job_manager = JobManager(self.state_db)  # type: ignore

    def check_labels(self) -> None:
        """Check data labels after linking."""

        with LoggingContext(
                logging.getLogger(), level=logging.INFO,
                handler=SQLLogHandler(self.state_db, self.job)), self.job.run(
                    self.job_manager, step=JobStep.check_labels) as rj:

            # TODO
            LOG.warning("The label checker would be run now...")

            rj.set_next_step(JobStep.write)
