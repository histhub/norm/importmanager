import logging
import typing

from contextlib import closing
from eavlib.graph import Graph
from tsv_loader.tsv_loader import TsvLoader  # type: ignore

from importmanager.db.job_manager import JobManager
from importmanager.db.schema_manager import SchemaManager
from importmanager.db.state_db import StateDBConnection
from importmanager.job import (Job, JobState, JobStep)
from importmanager.log import LoggingContext
from importmanager.log.sql import SQLLogHandler

LOG = logging.getLogger(__name__)


class LoadModule:
    def __init__(self, job: Job, state_db: StateDBConnection) -> None:
        """Initialize the LoadModule.

        :param job: the import job for which data should be loaded
        :param state_db: A StateDBConnection
        """
        if job.state in (JobState.finished, JobState.aborted):
            raise ValueError("Job %r is %s. No further steps are possible." % (
                job, job.state))

        if job.next_step is not JobStep.load:
            raise ValueError(
                "Job's next_step must be 'load'. "
                "The job %r has next_step '%s'." % (job, job.next_step))

        self.job = job
        self.state_db = state_db

        self.schema_manager = SchemaManager(self.state_db)  # type: ignore

    def create_schema(self) -> str:
        """Creates the temporary schema for self.job
        If self.job already has a temp. schema, it will be dropped and
        recreated.

        :returns: str -- the name of the temporary schema
        """

        temp_schema = self.schema_manager.schema_for_job(self.job)

        if temp_schema:
            # Drop schema
            self.schema_manager.drop_schema(self.job)

        # Generate fresh database schema
        return self.schema_manager.generate_schema_for_job(self.job)

    def load_model(self, model_file: str) -> int:
        """Load a model into self.job's temp. schema

        :param model_file: Path to the YAML file describing the model
        :returns: id -- the ID of the loaded model
        """
        # Load model
        LOG.info("Populating schema '%s' with model from '%s'...",
                 self.job.temp_schema, model_file)
        return self.schema_manager.load_model(self.job, model_file)

    def load_from_plan(self, load_plan_path: str, model_file: str,
                       verbose: bool = False) -> None:
        """Load the load plan into the job's schema.

        :param load_plan_path: The load plan to load
        :param model_file: Path to the YAML file describing the Graph model
        :param verbose: ???
        """

        job_manager = JobManager(self.state_db)  # type: ignore

        with LoggingContext(
                logging.getLogger(), level=logging.INFO,
                handler=SQLLogHandler(self.state_db, self.job)), \
                self.job.run(job_manager, step=JobStep.load) as rj:

            schema_name = self.create_schema()
            LOG.info("Using temporary schema '%s'" % schema_name)

            model_id = self.load_model(model_file)

            def _load_parser(module: str, cls: str) -> typing.Any:
                return getattr(__import__(
                    "tsv_loader.parsers.%s" % module,
                    fromlist=["tsv_loader.parsers"]), cls)

            parsers = dict(
                histhub_dates_parser=_load_parser(
                    "histhub_dates_parser", "histhub_dates_parser")(),
                histhub_intervals_parser=_load_parser(
                    "histhub_durations_parser",
                    "histhub_durations_parser")(),
                histhub_forenames_parser=_load_parser(
                    "histhub_names_parser", "histhub_forenames_parser")())
            LOG.debug("Loader parsers: %r", parsers)

            try:
                with closing(self.job.eav_connection(
                        self.state_db.copy())) as state_eav:
                    LOG.info("Running the TSV loader...")
                    graph = Graph(state_eav, model_id)

                    TsvLoader(
                        graph, load_plan_path, parsers_dict=parsers,
                        dbid_attr="hhb_id")
            except Exception:
                rj.set_next_step(JobStep.load)
                raise
            else:
                rj.set_next_step(JobStep.link)
