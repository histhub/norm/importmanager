import datetime
import logging

from contextlib import closing
from eavlib.graph import Graph
from importtools.common.linking_map import LinkingMap  # type: ignore
from importtools.linker.histhub_linker import HistHubLinker  # type: ignore
from normlib import HistHubID
from typing import (List, TYPE_CHECKING)

from importmanager.concordance import (Concordance, ConcordanceState)
from importmanager.job import (Job, JobState, JobStep)
from importmanager.db.concordance_manager import ConcordanceManager
from importmanager.db.job_manager import JobManager
from importmanager.db.state_db import StateDBConnection
from importmanager.log import LoggingContext
from importmanager.log.sql import SQLLogHandler
from importmanager.util import AutoFlushCache

LOG = logging.getLogger(__name__)
CONCORDANCE_BATCH_SIZE = 1000

if TYPE_CHECKING:
    from typing import TypeVar
    T = TypeVar("T")


class LinkerAlreadyRanError(Exception):
    pass


def _map_state(s: LinkingMap.State) -> ConcordanceState:
    if s is LinkingMap.State.unlinked:
        return ConcordanceState.new
    elif s is LinkingMap.State.linked:
        return ConcordanceState.linked
    raise TypeError(
        "Cannot map state %s to ConcordanceState" % s)


class LinkModule:
    def __init__(self, job: Job, state_db: StateDBConnection,
                 hhb_main: Graph) -> None:
        """Initialise the LinkModule.

        :param job: the import job which should get linked
        :param state_db: A StateDBConnection
        :param hhb_main: A Graph connected to the histHub main database
        """
        if job.state in (JobState.finished, JobState.aborted):
            raise ValueError("Job %r is %s. No further steps are possible." % (
                job, job.state))

        if job.next_step is not JobStep.link:
            raise ValueError(
                "Job's next_step must be 'link'. "
                "The job %r has next_step '%s'." % (job, job.next_step))

        self.job = job
        self.state_db = state_db
        self.hhb_main = hhb_main

        self.job_manager = JobManager(self.state_db)  # type: ignore
        self.concordance_manager = ConcordanceManager(  # type: ignore
            self.state_db)

    def link(self, force: bool = False) -> None:  # noqa: C901
        with LoggingContext(
                logging.getLogger(), level=logging.DEBUG,
                handler=SQLLogHandler(self.state_db, self.job)), \
                self.job.run(self.job_manager, step=JobStep.link,
                             singleton=True) as rj:
            # Check if there are concordances in the database already
            num_concordances = self.concordance_manager.count_concordances(
                self.job, include_resolved=True)
            if num_concordances > 0:
                if not force:
                    raise LinkerAlreadyRanError(
                        "There are already %u existing concordances" % (
                            num_concordances))

                # Delete all existing concordances
                self.concordance_manager.delete_concordances(
                    self.concordance_manager.list_concordances(
                        self.job, limit=None, include_resolved=True))

                if 0 < self.concordance_manager.count_concordances(
                        self.job, include_resolved=True):
                    raise RuntimeError(
                        "Failed to delete all existing concordances!")

                LOG.info("Existing concordances have been deleted.")

            # Run linker
            try:
                if not self.job.model_id:
                    raise ValueError(
                        "Job %r is missing a model_id!" % self.job)

                LOG.info("Linking...")

                with closing(self.job.eav_connection(
                        self.state_db.copy())) as staging_eav:
                    staging_graph = Graph(staging_eav, self.job.model_id)
                    linker = HistHubLinker(
                        histhub_graph=self.hhb_main,
                        staging_graph=staging_graph)

                    # Generate concordances
                    linking_map = linker.find_identifiable_concordances()

                LOG.debug("Got linking map from linker:\n%s", linking_map)
                LOG.info("Linking complete.")

                LOG.info("Generating concordances...")

                errors = False
                stored_concordances, total_concordances = 0, 0

                def _flush_concordances(items: List[Concordance]) -> None:
                    nonlocal stored_concordances
                    self.concordance_manager.store_concordances(items)
                    stored_concordances += len(items)
                    LOG.debug(
                        "Stored %u concordances. (total %u stored)",
                        len(items), stored_concordances)

                concordance_cache = AutoFlushCache(
                    _flush_concordances, max_len=CONCORDANCE_BATCH_SIZE,
                    max_age=datetime.timedelta(seconds=60))

                # Filter relevant entries from the linking map, create
                # concordances for them in batches (the link targets are object
                # IDs in histHub main and need to be resolved to histHub
                # IDs. This is done in batches for performance…) and write them
                # to the database in batches.

                # Filter out ref entries and intra links from linking map
                entries = list(filter(lambda entry: all((
                        entry.object_type is LinkingMap.ObjType.info,
                        entry.state is not LinkingMap.State.intra_link,
                    )), linking_map.entries()))

                # Cache value maps (because they have a node attribute) of the
                # hhb_id attribute to be used by hhb_id_of()
                hhb_id_maps = {
                    v.node.id: v for v in self.hhb_main.traversal().v(*(
                            e.eav_id for e in entries
                            if e.state is LinkingMap.State.linked
                        )).value_map("hhb_id")  # 0:1
                    }

                def hhb_id_of(node_id: int) -> HistHubID:
                    try:
                        return HistHubID(hhb_id_maps[node_id]["hhb_id"])
                    except KeyError:
                        raise RuntimeError(
                            "Object %u is missing a histHub ID" % node_id)

                total_concordances = len(entries)

                for entry in entries:
                    try:
                        # Make concordance
                        state = _map_state(entry.state)

                        concordance_cache.add(Concordance(
                            job=self.job,
                            staging_id=entry.stag_id,
                            candidates={},  # not implemented!
                            state=state,
                            link_to=(
                                hhb_id_of(entry.eav_id)
                                if state is ConcordanceState.linked
                                else None)))
                    except Exception:
                        errors = True
                        LOG.exception(
                            "Linking of staging ID %u failed",
                            entry.stag_id, exc_info=True)

                concordance_cache.flush()

                if errors:
                    LOG.info(
                        "Some (%u/%u) concordances have been stored in the "
                        "state DB.", stored_concordances, total_concordances)
                    raise RuntimeError(
                        "There were errors during linking! Please re-run.")
                else:
                    LOG.info("All (%u) concordances have been stored in the "
                             "state DB.", stored_concordances)

                # Count manual cases
                num_manual = len(self.concordance_manager.list_concordances(
                    self.job, include_resolved=False))

                if num_manual:
                    LOG.warning(
                        "Linking completed! %d cases for manual inspection.",
                        num_manual)
                else:
                    LOG.info(
                        "Linking completed! "
                        "All cases could be automatically linked.")
            except Exception:
                rj.set_next_step(JobStep.link)
                raise

            # on success no change in next_step

    def commit(self) -> None:
        if self.job.step is not JobStep.link:
            # Check if concordances are present (it can happen that when write
            # fails because of outdated concordances, the user is thrown back
            # to link and accepts the concordances without relinking)
            if not self.concordance_manager.count_concordances(
                    self.job, include_resolved=True):
                raise ValueError(
                    "Please run the linker for the job %r first!" % self.job)
            if self.concordance_manager.count_concordances(
                    self.job, include_resolved=False) > 0:
                raise ValueError(
                    "Please resolve all concordances for the job %r first!" % (
                        self.job))

        with self.job.run(self.job_manager, step=JobStep.link) as rj:
            rj.set_next_step(JobStep.write)
