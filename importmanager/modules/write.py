import datetime
import logging
import typing

from contextlib import closing
from eavlib.core.change_batch import ChangeBatch
from eavlib.core.data_manager import DataManager
from eavlib.core.id_manager import SequenceIDManager
from eavlib.graph import (Graph, Node)
from eavlib.gremlin.traversal import P
from importtools.common.linking_map import LinkingMap  # type: ignore
from importtools.linker.histhub_linker import HistHubLinker  # type: ignore
from importtools.writer.histhub_writer import HistHubWriter  # type: ignore
from normlib import HistHubID
from normlib.change import (Action, ChangedEntity)

from importmanager.concordance import ConcordanceState
from importmanager.job import (Job, JobState, JobStep)
from importmanager.db.concordance_manager import ConcordanceManager
from importmanager.db.job_manager import JobManager
from importmanager.db.schema_manager import SchemaManager
from importmanager.log import LoggingContext
from importmanager.log.sql import SQLLogHandler
from importmanager.provider import Provider
from importmanager.util import AutoFlushCache

if typing.TYPE_CHECKING:
    from normlib.connection import NormDBConnection
    from importmanager.db.state_db import StateDBConnection
    from typing import Dict

LOG = logging.getLogger(__name__)


def _find_im_provider(provider: Provider, graph: Graph) -> Node:
    """Finds the node matching the given ImportManager provider in graph.

    :param provider: The provider to find.
    :type provider: importmanager.provider.Provider
    :param graph: The graph to search in.
    :type graph: eavlib.graph.Graph
    :returns Node: The graph Node representing the provider/institution.
    :raises: StopIteration -- if no matching provider exists.
    """
    return next(  # type: ignore
        graph.traversal().v().has_label("Institution").has(
            "name", provider.name))


def _ensure_provider_exists(provider: Provider,
                            norm_db: "NormDBConnection") -> None:
    """Creates a new Institution object in graph unless it exists already.

    If it exists the "is_provider" attribute is set to "true" if needed.

    :param provider: The provider to check.
    :type provider: importmanager.provider.Provider
    :param graph: The graph to search/create in.
    :type graph: eavlib.graph.Graph
    """
    graph = norm_db.graph
    data_manager = DataManager(graph)
    model = data_manager.eav_model()
    change_batch = ChangeBatch()

    try:
        node = _find_im_provider(provider, graph)

        if next(graph.traversal().v(node).values(
                "is_provider"), None) == "true":
            LOG.info("Institution %s already known as a provider.",
                     provider.name)
            return  # nothing else to do
        else:
            # Institution exists, but is not a provider -> convert it
            institution_id = node.id
            LOG.info(
                "Institution %s already exists, turning into provider...",
                provider.name)
    except StopIteration:
        # No institution found -> create a new one
        LOG.info("Institution %s missing in histHub database. "
                 "Creating it...", provider.name)
        institution_id = SequenceIDManager(
            graph.connection, "object_id_seq").next_id()

        change_batch.write_object(
            institution_id,
            model.entity("Institution").id)
        change_batch.write_attribute(
            institution_id,
            model.entity("Identifiable").attribute("hhb_id").id,
            institution_id)
        change_batch.write_attribute(
            institution_id,
            model.entity("Institution").attribute("name").id,
            provider.name)

    # Convert institution to provider
    change_batch.write_attribute(
        institution_id,
        model.entity("Institution").attribute("is_provider").id,
        "true")

    data_manager.execute_batch(change_batch)
    data_manager.commit()


class ConcordanceStateError(Exception):
    pass


class WriteModule:
    def __init__(self, job: Job, state_db: "StateDBConnection",
                 norm_db: "NormDBConnection") -> None:
        """Initialise the WriteModule.

        :param job: the import job which should get merged into the main DB
        :type job: importmanager.job.Job
        :param state_db: A StateDBConnection
        :type state_db: importmanager.db.state_db.StateDBConnection
        :param norm_db: A NormDBConnection
        :type norm_db: normlib.connection.NormDBConnection
        """
        if job.state in (JobState.finished, JobState.aborted):
            raise ValueError("Job %r is %s. No further steps are possible." % (
                job, job.state))

        if job.next_step is not JobStep.write:
            raise ValueError(
                "Job's next_step must be 'write'. "
                "The job %r has next_step '%s'." % (job, job.next_step))

        self.job = job
        self.state_db = state_db
        self.norm_db = norm_db

        # ImportManager managers
        self.job_manager = JobManager(self.state_db)  # type: ignore
        self.concordance_manager = ConcordanceManager(  # type: ignore
            self.state_db)
        self.schema_manager = SchemaManager(self.state_db)  # type: ignore

        # NormDB managers
        self.revision_manager = self.norm_db.revision_manager()
        self.log_manager = self.norm_db.log_manager()
        self.dump_manager = self.norm_db.dump_manager()

    def _check_db_server_time(self) -> None:
        """Ensures that the system time of the database server does not deviate
        too much from the time on the machine running ImportManager.

        This is done in an effort to ensure that the modification times in the
        histHub database make sense.
        :raises RuntimeError: if the time deviation is too big.
        """
        with self.norm_db.graph.connection.cursor() as cursor:
            cursor.execute("SELECT clock_timestamp()")
            time = datetime.datetime.now(datetime.timezone.utc)
            dbtime = cursor.fetchone()[0]
        delta = abs(time - dbtime)

        if delta > datetime.timedelta(seconds=10):
            raise RuntimeError(
                "Time difference to database server is too large (%s). "
                "Please synchronise the clocks and try again." % delta)

    def write(self) -> None:  # noqa: C901
        try:
            self._check_db_server_time()
        except RuntimeError as e:
            raise RuntimeError(
                "Cannot start write process, because: %s" % e) from e

        with LoggingContext(
                logging.getLogger(), level=logging.DEBUG,
                handler=SQLLogHandler(self.state_db, self.job)):
            with self.job.run(self.job_manager, step=JobStep.write,
                              singleton=True) as rj:
                with closing(self.job.eav_connection(
                        self.state_db.copy())) as state_eav:
                    if not self.job.model_id:
                        raise ValueError("model_id IS NULL")
                    staging_graph = Graph(state_eav, self.job.model_id)

                    _ensure_provider_exists(self.job.provider, self.norm_db)

                    # Run linker before writing
                    LOG.info("Linking staging data against histHub data...")
                    linker = HistHubLinker(
                        histhub_graph=self.norm_db.graph,
                        staging_graph=staging_graph)
                    linking_map = linker.find_anchor_concordances()
                    LOG.debug("Got linking map from linker:\n%s", linking_map)

                    try:
                        # Load concordances
                        LOG.info("Fetching stored concordances...")
                        concordance_map = {
                            c.staging_id: c for c in
                            self.concordance_manager.list_concordances(
                                self.job, include_resolved=True)}

                        if not all(c.state in (
                                ConcordanceState.new, ConcordanceState.linked
                                ) for c in concordance_map.values()):
                            raise ConcordanceStateError(
                                "All concordances must be either new or "
                                "linked for the writing process to start")

                        hhb_traversal = self.norm_db.graph.traversal()

                        hhb_ids = {
                            c.link_to
                            for c in concordance_map.values()
                            if c.state is ConcordanceState.linked
                            and c.link_to is not None}

                        hhb_eav_id_map = {}  # type: Dict[int, int]
                        if hhb_ids:
                            hhb_eav_id_map.update(
                                (int(v["hhb_id"]), v.node.id)
                                for v in hhb_traversal.v().has(
                                    "hhb_id", P.within(*hhb_ids))
                                .value_map("hhb_id"))

                        # Checking "clean" concordances from state DB for
                        # changes…

                        # If the histHub main database has been updated since
                        # linking, the correct linking decisions might have
                        # changed.
                        LOG.info(
                            "Checking if stored concordances are still "
                            "applicable to current histHub state and updating "
                            "linking map.")
                        identifiables = linker.identifiable_entities

                        num_outdated_concordances = 0
                        num_concordance_errors = 0

                        for entry in linking_map.entries():
                            if entry.state is LinkingMap.State.intra_link \
                                or entry.object_type is \
                                    not LinkingMap.ObjType.info:
                                continue

                            try:
                                concordance = concordance_map[entry.stag_id]
                            except KeyError:
                                if entry.entity_name not in identifiables:
                                    # Not an entry we care about, because the
                                    # user can only modify concordances of
                                    # identifiable entities.
                                    continue
                                raise ConcordanceStateError(
                                    "Missing concordance for staging node %u. "
                                    "Has the temp. schema been modified??" % (
                                        entry.stag_id))

                            if concordance.dirty:
                                # User has modified concordance -> update map
                                if concordance.state is \
                                   ConcordanceState.linked:
                                    if concordance.link_to is None:
                                        LOG.error(
                                            "Concordance %u (staging ID: %u) "
                                            "has state %r but no histHub ID "
                                            "to link", concordance.id,
                                            concordance.staging_id,
                                            concordance.state)
                                        num_concordance_errors += 1
                                        continue

                                    entry.state = LinkingMap.State.linked
                                    entry.eav_id = hhb_eav_id_map[
                                        entry.histhub_id]
                                elif concordance.state is ConcordanceState.new:
                                    if concordance.link_to is not None:
                                        LOG.error(
                                            "Concordance %u (staging ID: %u) "
                                            "has state %r but also a "
                                            "histHub ID (%u) to link",
                                            concordance.id,
                                            concordance.staging_id,
                                            concordance.state,
                                            concordance.link_to)
                                        num_concordance_errors += 1
                                        continue
                                    entry.state = LinkingMap.State.unlinked
                                    entry.eav_id = None
                                else:
                                    # This must not happen!
                                    LOG.error(
                                        "Oops. There is an error in the "
                                        "concordances. Concordance %u has "
                                        "inacceptable state %r!",
                                        concordance.id, concordance.state)
                                    num_concordance_errors += 1

                                continue
                            # FIXME: Also compare link_to…
                            elif entry.state is LinkingMap.State.linked \
                                    and concordance.state is not \
                                    ConcordanceState.linked:
                                concordance.state = ConcordanceState.linked
                                concordance.link_to = HistHubID(next(
                                    hhb_traversal.v(entry.eav_id)
                                    .values("hhb_id")))
                            elif entry.state is LinkingMap.State.unlinked \
                                    and concordance.state is not \
                                    ConcordanceState.new:
                                concordance.state = ConcordanceState.new
                                concordance.link_to = None
                            else:
                                # Concordance and linking map state still match
                                continue

                            LOG.warning(
                                "Concordance for staging ID %u has been "
                                "updated to %r",
                                concordance.staging_id, concordance.state)
                            self.concordance_manager.store_concordance(
                                concordance)
                            num_outdated_concordances += 1

                        if num_outdated_concordances:
                            # Must return to link step, because histHub main DB
                            # has changed since the last linking run.
                            raise ConcordanceStateError(
                                "There are %u inconsistencies between the "
                                "stored concordances and the current histHub "
                                "state.\n"
                                "The concordances have been updated with the "
                                "linker's decisions and must thus be verified "
                                "manually.\n"
                                "The process has been reset to linking." % (
                                    num_outdated_concordances))
                        if num_concordance_errors:
                            raise ConcordanceStateError(
                                "%u errors have been found in the stored "
                                "concordances.\nThese errors must be fixed "
                                "before the import can conclude.\n"
                                "The process has been reset to linking." % (
                                    num_concordance_errors))
                    except ConcordanceStateError:
                        LOG.warning("Reverting to link step.")
                        rj.set_next_step(JobStep.link)
                        raise

                    LOG.debug("Merged linking map:\n%s", linking_map)

                    # Prepare change cache
                    change_cache = AutoFlushCache(
                        self.log_manager.checkin_changes,
                        max_len=25,
                        max_age=datetime.timedelta(seconds=2))

                    def cache_change(
                            obj_id: int, entity_name: str, hhb_id: int,
                            action: Action, message: str) -> None:
                        if hhb_id and hhb_id > 0:
                            change_cache.add(ChangedEntity(
                                histhub_id=hhb_id, action=action,
                                message=message, revision=self.revision))
                        elif entity_name in identifiables:
                            LOG.error("Object %s(%u): no histHub ID received "
                                      "from writer for identifiable object.",
                                      entity_name, obj_id)

                    # Request new revision
                    self.revision = self.revision_manager.create_revision(
                        "Import: %s (of %s)" % (
                            self.job.name, self.job.provider.name))

                    LOG.info("Requested new revision %u.", self.revision.id)

                    try:
                        # FIXME: Handle exceptions
                        LOG.info("Writing data to histHub database...")
                        writer = HistHubWriter(
                            self.norm_db, staging_graph, linking_map,
                            self.job.provider.name)

                        # Set hooks to update revision log
                        writer.register_hook(
                            HistHubWriter.HOOKS.ENTITY_INSERTION,
                            lambda *, object_id, entity_name, histhub_id: cache_change(  # noqa: E501
                                object_id, entity_name, histhub_id,
                                Action.CREATED,
                                "Add new %s entity" % entity_name))

                        writer.register_hook(
                            HistHubWriter.HOOKS.ENTITY_UPDATE,
                            lambda *, object_id, entity_name, histhub_id: cache_change(  # noqa: E501
                                object_id, entity_name, histhub_id,
                                Action.MODIFIED,
                                "Update %s entity" % entity_name))

                        writer.run()

                        # Write all remaining changes to revision log
                        change_cache.flush()

                        # Store a snapshot of the changed entities in the
                        # sidecar DB.
                        LOG.info("Taking snapshot of new revision...")
                        self.dump_manager.snapshot_entities(
                            [ce.histhub_id for ce in
                             self.log_manager.show_revision(self.revision)])
                    except Exception:
                        self.revision_manager.rollback_revision(self.revision)
                        LOG.error("Revision %u has been rolled back because "
                                  "an exception occurred.", self.revision.id)
                        raise
                    else:
                        self.revision_manager.commit_revision(self.revision)
                        LOG.info("Committed revision %u", self.revision.id)

                        self.job.revision_id = self.revision.id
                        self.job_manager.store_job(self.job)

                        # TODO: Maybe run ANALYZE? VACUUM? REINDEX?

            self.job.step = JobStep.completed
            self.job.next_step = JobStep.none
            self.job.state = JobState.finished
            self.job_manager.store_job(self.job)
            LOG.info("Import completed!")

            # Drop schema
            self.schema_manager.drop_schema(self.job)
