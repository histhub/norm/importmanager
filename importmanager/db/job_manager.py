import abc

from typing import (Any, Dict, Generator, List, Optional, Tuple, Type)

from importmanager.job import (Job, JobState, JobStep)
from importmanager.provider import Provider
from importmanager.db.provider_manager import ProviderManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)


class JobManager(metaclass=abc.ABCMeta):
    def __new__(cls, state_db: StateDBConnection, *args: Any, **kwargs: Any) \
            -> "JobManager":
        """Creates a new JobManager based on the provided DB connection.

        :param state_db: A StateDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: JobManager -- a new JobManager instance
        """

        if cls is not JobManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")

        real_cls = None  # type: Optional[Type[JobManager]]
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from importmanager.db.pgsql.job_manager import PgSQLJobManager
            real_cls = PgSQLJobManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from importmanager.db.sqlite.job_manager import SQLiteJobManager
        #     real_cls = SQLiteJobManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    def _dict_to_job(self, job_dict: Dict[str, Any]) -> Job:
        """Creates a Job object from a dict from the DB.

        :param job_dict: the DB row as a dict
        :type job_dict: Dict[str, Any]
        :returns: importmanager.job.Job -- the job
        """

        def _argify(job_dict: Dict[str, Any]) \
                -> Generator[Tuple[str, Any], None, None]:
            for k, v in job_dict.items():
                if k == "provider_id":
                    k = "provider"
                    v = self.provider_manager.get_by_id(v)
                elif k == "state":
                    v = JobState(v)
                elif k in ("step", "next_step"):
                    v = JobStep(v)

                yield (k, v)

        args = dict(_argify(job_dict))
        return Job(**args)

    def _job_to_dict(self, job: Job) -> Dict[str, Any]:
        """Convert a Job object to a dict that can be stored in the DB"""

        def _conv(job: Job) -> Generator[Tuple[str, object], None, None]:
            for k, v in job:
                if isinstance(v, Provider):
                    k = "provider_id"
                    v = v.id
                elif isinstance(v, JobState):
                    v = v.value
                elif isinstance(v, JobStep):
                    v = v.value
                yield (k, v)

        return dict(_conv(job))

    @abc.abstractmethod
    def __init__(self, state_db: StateDBConnection) -> None:
        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")
        self.db = state_db
        self.provider_manager = ProviderManager(self.db)  # type: ignore

    @abc.abstractmethod
    def create_job(self, name: str, provider: Provider) -> Job:
        """Creates a new Job.

        :param name: the name of the Job
        :type name: str
        :param provider: the provider whose data is to be imported
        :type provider: Provider
        :returns: Job -- the newly created job
        """

    @abc.abstractmethod
    def list_jobs(self, exclude_states: Tuple[JobState, ...] = ()) \
            -> List[Job]:
        """Fetches a list of all available Jobs in the state database.

        NOTE: The order in which the jobs are returned is arbitrary.

        :param exclude_states: do not return jobs in these states
        :type exclude_states: Tuple[JobState]
        :returns: List[Job] -- the list of Job
        """

    @abc.abstractmethod
    def cancel_job(self, job: Job) -> None:
        """Cancels a Job.

        This method will always try to delete the temp_schema when the database
        has a value set.  Apart from that, this method does nothing when the
        job is already cancelled.

        :param job: the job to cancel
        :type job: Job
        :raises ValueError: when the Job has no ID, thus cannot be updated in
        the database
        :raises RuntimeError: when the database failed to delete the
        temporary schema of the Job.
        """

    @abc.abstractmethod
    def store_job(self, job: Job) -> None:
        """Stores an updated Job object to the database.
        This method will do nothing, if no Job exists with a matching ID.

        :param job: the job to store
        :type job: Job
        """

    def update_last_action(self, job: Job) -> None:
        """Updates the last_action value of `job`.

        :param job: the job to update last_action for
        :type job: Job
        """

    @abc.abstractmethod
    def get_by_id(self, job_id: int) -> Job:
        """Fetches a Job from the state database by ID

        :param job_id: the ID to fetch
        :type job_id: int
        :returns: Job -- the job (when found)
        :raises ValueError: if no job with `job_id` exists
        """

    @abc.abstractmethod
    def get_by_name(self, job_name: str) -> Job:
        """Fetches a Job from the state database by name

        :param job_name: the name to fetch
        :type job_name: str
        :returns: Job -- the job (when found)
        :raises ValueError: if no job with `job_name` exists
        """
