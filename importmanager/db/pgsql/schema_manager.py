import logging
import eavlib.core

from psycopg2 import sql  # type: ignore
from psycopg2.extensions import ISOLATION_LEVEL_SERIALIZABLE  # type: ignore
from typing import Optional

from importmanager.db.job_manager import JobManager
from importmanager.db.schema_manager import SchemaManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)
from importmanager.job import Job

LOG = logging.getLogger(__name__)


class PgSQLSchemaManager(SchemaManager):
    def __init__(self, state_db: StateDBConnection) -> None:
        if state_db.backend is not StateDBBackend.POSTGRESQL:
            raise TypeError(
                "state_db must be a PostgreSQL backed StateDBConnection")
        super().__init__(state_db)
        self.job_manager = JobManager(self.db)  # type: ignore

    def _schema_exists(self, schema_name: str) -> bool:
        """Checks if a schema exists in the database

        :param schema_name: Name of the schema to check the existence of
        :type schema_name: str
        :returns: bool -- true if a schema with `schema_name` exists
        """
        with self.db.cursor() as cursor:
            cursor.execute(
                """
                SELECT 1
                FROM information_schema.schemata
                WHERE schema_name = %s""", (schema_name,))
            return cursor.fetchone() is not None

    def schema_for_job(self, job: Job) -> Optional[str]:
        with self.db.cursor() as cursor:
            cursor.execute("SELECT temp_schema FROM job WHERE id = %s",
                           (job.id,))
            row = cursor.fetchone()
            return row[0] if row else None

    def load_model(self, job: Job, path: str) -> int:
        if job.temp_schema is None:
            raise ValueError(
                "Job '%s' does not have a temporary schema (yet)" % job)

        LOG.debug("Loading model from file '%s' into schema '%s'",
                  path, job.temp_schema)

        model_description = eavlib.core.model_description.load_model_from_yaml(
            path)
        eavlib.core.model_description.check_model_description(
            model_description)

        eav_connection = job.eav_connection(self.db)

        model_manager = eavlib.core.model_manager.ModelManager(eav_connection)

        # Write model description
        model_id = model_manager.write(model_description)  # type: int

        LOG.info("Successfully loaded model '%s' (id=%d) into schema %s",
                 model_description.name, model_id, job.temp_schema)

        job.model_id = model_id
        self.job_manager.store_job(job)

        return model_id

    def generate_schema_for_job(self, job: Job) -> str:
        # Ensure that the job doesn't already have a temporary schema
        if self.schema_for_job(job):
            raise RuntimeError(
                "Temporary schema for job %s already exists" % job)

        # Create temporary schema
        schema_name = ""
        while True:
            schema_name = SchemaManager._generate_schema_uuid()

            if self._schema_exists(schema_name):
                # A duplicate UUID has been generated
                LOG.debug(
                    "Duplicate schema name was generated: %s" % schema_name)
                continue
            break

        if not schema_name:
            # This should never occur
            raise RuntimeError("Could not generate a random schema name")

        old_isolation_level = self.db.connection.isolation_level
        try:
            # Execute schema creation in SERIALIZABLE isolation mode

            self.db.commit()
            self.db.connection.isolation_level = ISOLATION_LEVEL_SERIALIZABLE

            # Create schema
            with self.db.cursor() as cursor:
                cursor.execute(sql.SQL("CREATE SCHEMA {}").format(
                    sql.Identifier(schema_name)))
            self.db.commit()
            LOG.debug("Created schema %s", schema_name)

            job.temp_schema = schema_name

            # Format schema (load tables)
            schema_version = 0

            def _post_hook(version: int, num_scripts_executed: int) -> None:
                nonlocal schema_version
                schema_version = version
                LOG.debug(
                    "Schema migrated to version %u (executed %u %s)",
                    version, num_scripts_executed,
                    ("scripts" if num_scripts_executed != 1 else "script"))

            storage_manager = eavlib.core.storage_manager.StorageManager(
                job.eav_connection(self.db))
            storage_manager.migrator.register_hooks(after_migration=_post_hook)
            storage_manager.create()
            storage_manager.migrate()

            LOG.info("Schema '%s' has been formatted with version %u",
                     schema_name, schema_version)

            # Store temporary schema name
            self.job_manager.store_job(job)
        except Exception:
            self.db.rollback()
            raise
        finally:
            self.db.connection.isolation_level = old_isolation_level

        return schema_name

    def _drop_schema(self, schema_name: str) -> None:
        if not self._schema_exists(schema_name):
            raise ValueError("No schema with name '%s' exists" % schema_name)

        with self.db.cursor() as cursor:
            cursor.execute(
                sql.SQL("DROP SCHEMA {} CASCADE").format(
                    sql.Identifier(schema_name)))
        self.db.commit()

    def drop_schema(self, job: Job) -> None:
        if not job.temp_schema:
            raise ValueError("Job %r has no temporary schema assigned" % job)

        LOG.info(
            "Dropping temp. schema (%s) of job %r...", job.temp_schema, job)

        try:
            # Drop schema
            self._drop_schema(job.temp_schema)
        except ValueError as e:
            LOG.error("Could not drop temp. schema for job %r, because: %s. "
                      "Unsetting temp. schema nevertheless.", job, e)

        # Unset temp_schema for job
        with self.db.cursor() as cursor:
            cursor.execute("UPDATE job SET temp_schema = NULL WHERE id = %s",
                           (job.id,))
        self.db.commit()
