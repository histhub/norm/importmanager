import pathlib
import pkg_resources
import psycopg2  # type: ignore
import psycopg2.extras  # type: ignore

from typing import TYPE_CHECKING

from importmanager import __package_name__
from importmanager.db.migrator import SchemaMigrator
from importmanager.db.state_db import StateDBConnection

if TYPE_CHECKING:
    from typing import (List, TypeVar)
    T = TypeVar("T")


# NOTE: All usages of schema in this class should use "public".
#       This is because the ImportManager will schemas for all import jobs.  To
#       avoid name conflicts, the public schema should always be used for the
#       state DB.
class PgSQLSchemaMigrator(SchemaMigrator):
    migrations_dir = pkg_resources.resource_filename(
        __package_name__, "files/db/pgsql")

    def __init__(self, state_db: StateDBConnection, debug: bool) -> None:
        super().__init__(state_db, debug)

        self.state_db = state_db

        # Define a custom wait callback here to allow the script to be aborted
        # by SIGINT.
        psycopg2.extensions.set_wait_callback(psycopg2.extras.wait_select)

        if debug:
            class PgNoticesWriter(List["T"]):
                def append(self, x: "T") -> None:
                    super().append(x)
                    print(x, end="")
            self.state_db.connection.notices = PgNoticesWriter()

        self.state_db.connection.isolation_level = \
            psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE

        with self.state_db.cursor() as cursor:
            cursor.execute("SET default_with_oids = false;")

    def create(self) -> None:
        with self.state_db.cursor() as cursor:
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS public.version (
                schema_version integer NOT NULL
            );
            """)

            cursor.execute("SELECT schema_version FROM public.version")
            has_current_version = cursor.fetchone() is not None

            # Only set the version to 0 if the version table is empty.
            # NOTE: The version table could be non-empty if create() is called
            # on a pre-initialised schema.
            if not has_current_version:
                cursor.execute("""
                TRUNCATE TABLE public.version;
                INSERT INTO public.version (schema_version) VALUES (%s);
                """, (0,))

        self.state_db.commit()

    def create_schema(self) -> None:
        with self.state_db.cursor() as cursor:
            cursor.execute("CREATE SCHEMA IF NOT EXISTS public;")
        self.state_db.commit()

    def schema_exists(self) -> bool:
        with self.state_db.cursor() as cursor:
            cursor.execute("""
            SELECT 1
            FROM information_schema.schemata
            WHERE schema_name = %s;
            """, ("public",))
            return cursor.fetchone() is not None

    def is_created(self) -> bool:
        with self.state_db.cursor() as cursor:
            cursor.execute("""
            SELECT 1
            FROM information_schema.tables
            WHERE table_schema = %s AND table_name = 'version';
            """, ("public",))
            return cursor.fetchone() is not None

    def destroy(self) -> None:
        with self.state_db.cursor() as cursor:
            cursor.execute("""
            DROP SCHEMA public CASCADE;
            CREATE SCHEMA public;
            """)
        self.state_db.commit()

    def current_version(self) -> int:
        with self.state_db.cursor() as cursor:
            cursor.execute("SELECT schema_version FROM public.version")
            return int(cursor.fetchone()[0])

    # flake8: noqa C908
    def run_migration(self, version: int) -> None:
        import psycopg2.extensions  # type: ignore
        ISOL_SERIALIZABLE = psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE

        connection = self.state_db.connection
        num_scripts_executed = 0

        assert not connection.readonly
        assert not connection.autocommit

        print("Migrating to version %d…" % version)
        for _ in range(5):  # up to 5 retries in case of serialization failure
            old_isolation_level = connection.isolation_level
            try:
                # Make sure we're on a fresh transaction before migrating
                self.state_db.commit()

                if old_isolation_level is not ISOL_SERIALIZABLE:
                    connection.isolation_level = ISOL_SERIALIZABLE

                # Find all migration scripts for this version and execute them
                # in alphabetical order
                for scr_path in sorted(pathlib.Path(
                        self.migrations_dir).glob("%04u_*.sql" % version)):
                    with self.state_db.cursor() as cursor:
                        try:
                            print("- %s" % scr_path.name)
                            with open(scr_path, "r", encoding="utf-8") as scr:
                                sql = scr.read().format(SCHEMA_NAME="public")
                                cursor.execute(sql)
                        except Exception:
                            self.state_db.rollback()
                            raise

                    num_scripts_executed += 1

                if not num_scripts_executed:
                    raise ValueError(
                        "No migration to version %u found." % version)

                # Bump schema version if all scripts have successfully applied
                with self.state_db.cursor() as cursor:
                    cursor.execute(
                        """
                        TRUNCATE TABLE public.version;
                        INSERT INTO public.version (schema_version) VALUES (%s);
                        """, (version,))
                self.state_db.commit()
            except psycopg2.OperationalError as e:
                self.state_db.rollback()
                if e.pgcode != psycopg2.errorcodes.SERIALIZATION_FAILURE:
                    raise
                # silent retry
            except Exception:
                print("Migration failed!")
                self.state_db.rollback()
                raise
            else:
                break  # break to not trigger for's else
            finally:
                # Reset connection isolation level if needed
                if connection.isolation_level is not old_isolation_level:
                    connection.isolation_level = old_isolation_level
        else:
            raise RuntimeError("Maximum number of retries exhausted")
