import logging

from psycopg2.extras import DictCursor  # type: ignore
from typing import List

from importmanager.db.provider_manager import ProviderManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)
from importmanager.provider import Provider

LOG = logging.getLogger(__name__)


class PgSQLProviderManager(ProviderManager):
    def __init__(self, state_db: StateDBConnection) -> None:
        if state_db.backend is not StateDBBackend.POSTGRESQL:
            raise TypeError(
                "state_db must be a PostgreSQL backed StateDBConnection")
        super().__init__(state_db)

    def create_provider(self, name: str) -> Provider:
        with self.db.cursor() as cursor:
            cursor.execute(
                """
                INSERT INTO provider (name)
                VALUES (%s)
                RETURNING id""", (name,))
            created_id = cursor.fetchone()[0]
        self.db.commit()

        return Provider(name, id=created_id)

    def list_providers(self) -> List[Provider]:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute("SELECT id, name FROM provider")
            providers = cursor.fetchall()

        return list(map(lambda p: Provider(**dict(p)), providers))

    def get_by_id(self, provider_id: int) -> Provider:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                "SELECT id, name FROM provider WHERE id = %s", (provider_id,))
            row = cursor.fetchone()

        if not row:
            raise ValueError(
                "There exists no provider with ID %d" % provider_id)

        return Provider(**dict(row))

    def get_by_name(self, provider_name: str) -> Provider:
        with self.db.cursor() as cursor:
            cursor.execute(
                "SELECT id FROM provider WHERE name = %s", (provider_name,))
            row = cursor.fetchone()

        if not row:
            raise ValueError(
                "There exists no provider with name %s" % provider_name)

        return self.get_by_id(row[0])
