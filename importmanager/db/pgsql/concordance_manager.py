import collections
import logging

from psycopg2.extras import (  # type: ignore
    DictCursor, Json, execute_batch, execute_values)
from psycopg2 import sql  # type: ignore
from typing import (
    Any, Dict, Iterable, Iterator, List, Optional, Sequence, Tuple)

from importmanager.concordance import Concordance
from importmanager.job import Job
from importmanager.db.concordance_manager import ConcordanceManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)

LOG = logging.getLogger(__name__)


class PgSQLConcordanceManager(ConcordanceManager):
    def __init__(self, state_db: StateDBConnection) -> None:
        if state_db.backend is not StateDBBackend.POSTGRESQL:
            raise TypeError(
                "state_db must be a PostgreSQL backed StateDBConnection")
        super().__init__(state_db)

    def _concordance_to_pgdict(self, concordance: Concordance) \
            -> Dict[str, Any]:
        def _pgsqlify(prop: Any) -> Any:
            if isinstance(prop, dict):
                # Wrap dicts in Json() to work around "can't adapt type 'dict'"
                return Json(prop)
            else:
                return prop

        return {k: _pgsqlify(v) for k, v in
                self._concordance_to_dict(concordance).items()}

    def fetch_concordance(self, job: Job, id: int) -> Concordance:
        query = """
        SELECT *
        FROM concordance
        WHERE import_id = %(import_id)s AND id = %(id)s
        """
        params = dict(import_id=job.id, id=id)

        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, params)
            row = cursor.fetchone()
            if not row:
                raise RuntimeError(
                    "No concordance with ID %d for import %r" % (id, job))

            return self._dict_to_concordance(dict(row))

    def list_concordances(self, job: Job, limit: Optional[int] = None,
                          include_resolved: bool = False) -> List[Concordance]:
        query = "SELECT * FROM concordance WHERE import_id = %(import_id)s"
        params = dict(import_id=job.id, limit=limit)

        if not include_resolved:
            query += " AND state = 'undecided'"
        if limit:
            query += " LIMIT %(limit)s"

        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, params)
            return [self._dict_to_concordance(dict(row)) for row in cursor]

    def count_concordances(self, job: Job,
                           include_resolved: bool = False) -> int:
        query = """
        SELECT COUNT(*)
        FROM concordance
        WHERE import_id = %(import_id)s
        """
        params = dict(import_id=job.id)

        if not include_resolved:
            query += " AND state = 'undecided'"

        with self.db.cursor() as cursor:
            cursor.execute(query, params)
            return int(cursor.fetchone()[0])

    def delete_concordances(self, concordances: Sequence[Concordance]) -> int:
        c_map = {c.id: c for c in concordances if c.id is not None}
        count = 0
        with self.db.cursor() as cursor:
            cursor.execute(
                """
                DELETE FROM concordance
                WHERE id IN %s
                RETURNING id
                """,
                (tuple(c_map.keys()),))
            for row in cursor:
                c_map[row[0]].id = None
                count += 1
        self.db.commit()
        return count

    def store_concordance(self, concordance: Concordance) -> None:
        if not concordance:
            return

        props, values = zip(*filter(
            lambda item: item[0] != "id",
            self._concordance_to_pgdict(concordance).items()))
        db_id = None

        if concordance.id:
            with self.db.cursor() as cursor:
                # ID check: The (import_id, staging_id) pair ought to be
                # unique.  The following check asserts that concordance.id
                # matches one* of the IDs already in the state DB if present to
                # prevent the creation of duplicates.
                # *there could already be duplicates
                cursor.execute("""
                SELECT id
                FROM concordance
                WHERE import_id = %s AND staging_id = %s""", (
                    concordance.job.id, concordance.staging_id))
                db_ids = [row[0] for row in cursor]

            if db_ids and concordance.id not in db_ids:
                raise ValueError(
                    "Concordance %r cannot be stored. Its ID %u does not "
                    "match any of the ones already in the state DB %r",
                    concordance, concordance.id, db_ids)

            # Update concordance already in DB.
            db_id = concordance.id

        if db_id:
            # UPDATE
            query = sql.SQL("""
            UPDATE concordance
            SET ({}) = %(props)s
            WHERE id = %(id)s
            RETURNING id
            """).format(sql.SQL(", ").join(map(sql.Identifier, props)))
        else:
            # INSERT
            query = sql.SQL("""
            INSERT INTO concordance ({})
            VALUES %(props)s
            RETURNING id
            """).format(sql.SQL(", ").join(map(sql.Identifier, props)))

        with self.db.cursor() as cursor:
            cursor.execute(query, {"props": values, "id": db_id})
            db_id = int(cursor.fetchone()[0])

            LOG.debug("Stored properties for concordance id %r: %s",
                      db_id, dict(zip(props, values)))

        self.db.commit()
        concordance.id = db_id

    def __batch_insert_concordances(
            self, concordances: Iterable[Concordance]) \
            -> List[Tuple[int, int, int]]:
        # new_ids = [(id, staging_id, import_id), ...]
        new_ids = []  # type: List[Tuple[int, int, int]]

        def _batch_insert(
                keys: Iterable[str], dicts: List[Dict[str, Any]]) \
                -> Iterator[Tuple[int, int, int]]:
            keys = tuple(keys)
            with self.db.cursor() as cursor:
                execute_values(
                    cursor, sql.SQL("""
                    INSERT INTO concordance ({})
                    VALUES %s
                    RETURNING id, staging_id, import_id
                    """).format(sql.SQL(", ").join(map(sql.Identifier, keys))),
                    dicts,
                    template="(%s)" % ", ".join("%%(%s)s" % k for k in keys))
                return cursor.fetchall()  # type: ignore

        groups = collections.defaultdict(list) \
            # type: Dict[Tuple[str, ...], List[Dict[str, Any]]]
        for props in map(self._concordance_to_pgdict, concordances):
            del props["id"]
            groups[tuple(props.keys())].append(props)

        for keys, dicts in groups.items():
            new_ids += _batch_insert(keys, dicts)

        return new_ids

    def __patch_concordance_ids(self, concordances: Sequence[Concordance],
                                id_tbl: List[Tuple[int, int, int]]) -> None:
        # id_tbl = [(id, staging_id, import_id), ...]

        # Update concordances with stored IDs
        c_map = {(c.staging_id, c.job.id): c for c in concordances}
        for i in id_tbl:
            c_map[i[1:3]].id = int(i[0])

    def __batch_update_concordances(
            self, concordances: Sequence[Concordance]) -> None:
        if not concordances:
            return

        def _batch_update(keys: Iterable[str],
                          dicts: List[Dict[str, Any]]) -> None:
            keys = list(keys)
            keys.remove("id")
            # id must be first because of argument order in prepared stmt
            keys.insert(0, "id")

            with self.db.cursor() as cursor:
                cursor.execute(sql.SQL("""
                PREPARE batch_concordance_update AS
                UPDATE concordance SET ({}) = ({})
                WHERE id = $1
                """).format(
                    sql.SQL(", ").join(map(sql.Identifier, keys[1:])),
                    sql.SQL(", ").join(
                        sql.SQL("$%u" % (i+2)) for i in range(len(keys) - 1))))
                execute_batch(
                    cursor,
                    sql.SQL("EXECUTE batch_concordance_update({})").format(
                        sql.SQL(", ").join(sql.Placeholder(k) for k in keys)),
                    dicts)
                cursor.execute("DEALLOCATE batch_concordance_update")

        groups = collections.defaultdict(list) \
            # type: Dict[Tuple[str, ...], List[Dict[str, Any]]]
        for props in map(self._concordance_to_pgdict, concordances):
            groups[tuple(props.keys())].append(props)

        for keys, dicts in groups.items():
            _batch_update(keys, dicts)

    def store_concordances(self, concordances: Sequence[Concordance]) -> None:
        if not concordances:
            return

        with self.db.cursor() as cursor:
            # ID check: The (import_id, staging_id) pair ought to be unique.
            # The following check asserts that concordances' IDs matches at
            # least one* of the IDs already in the state DB for the respecting
            # pair if one is present to prevent the creation of duplicates.
            # *there could already be duplicates
            ids_in_db = collections.defaultdict(list)  \
                # type: Dict[Tuple[int, int], List[int]]
            execute_values(
                cursor, """
                SELECT
                  c.id, c.import_id, c.staging_id
                FROM
                  concordance c,
                  (VALUES %s) v (job_id, staging_id)
                WHERE c.import_id = v.job_id
                  AND c.staging_id = v.staging_id
                """, tuple((c.job.id, c.staging_id) for c in concordances),
                "(%s, %s)")
            for c_id, import_id, staging_id in cursor:
                ids_in_db[(import_id, staging_id)].append(c_id)

        to_insert, to_update, fails = [], [], []
        if ids_in_db:
            # Split up concordances in UPDATE and INSERT groups
            for c in concordances:
                db_ids = ids_in_db.get((c.job.id, c.staging_id), [])
                if db_ids:
                    if c.id and c.id not in db_ids:
                        LOG.error(
                            "Concordance %r cannot be stored. "
                            "Its ID %u does not match any of the ones "
                            "already in the state DB %r.",
                            c, c.id, db_ids)
                        fails.append(c)
                        continue
                    elif not c.id:
                        # XXX: Is this a good idea or should there be
                        #      some better algorithm to decide which of
                        #      the IDs to overwrite, even though there
                        #      should at most be one candidate??
                        LOG.debug(
                            "Trying to store concordance %r without ID but "
                            "there is a match in the state DB. Setting ID to "
                            "%u", c, db_ids[0])
                        c.id = db_ids[0]
                    to_update.append(c)
                else:
                    to_insert.append(c)
        else:
            # All inserts
            to_insert += concordances

        self.__batch_update_concordances(to_update)
        new_ids = self.__batch_insert_concordances(to_insert)
        self.db.commit()

        self.__patch_concordance_ids(to_insert, new_ids)

        if fails:
            raise ValueError(
                "%u concordances could not be stored!" % len(fails))
