import logging

from collections import OrderedDict
from psycopg2.extras import DictCursor  # type: ignore
from psycopg2 import sql  # type: ignore
from typing import (List, Tuple)

from importmanager.job import (Job, JobState)
from importmanager.provider import Provider
from importmanager.db.job_manager import JobManager
from importmanager.db.schema_manager import SchemaManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)

LOG = logging.getLogger(__name__)


class PgSQLJobManager(JobManager):
    def __init__(self, state_db: StateDBConnection) -> None:
        if state_db.backend is not StateDBBackend.POSTGRESQL:
            raise TypeError(
                "state_db must be a PostgreSQL backed StateDBConnection")
        super().__init__(state_db)

    def create_job(self, name: str, provider: Provider) -> Job:
        if not Job._is_valid_name(name):
            raise ValueError("Name '%s' is not permissible" % name)

        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                """
                INSERT INTO job (name, provider_id, step, next_step)
                VALUES (%s, %s, %s, %s)
                RETURNING *
                """, (name, provider.id, "none", "load"))
            row = cursor.fetchone()
        self.db.commit()

        return self._dict_to_job(row)

    def list_jobs(self, exclude_states: Tuple[JobState, ...] = ()) \
            -> List[Job]:
        query = "SELECT * FROM job"
        params = {}
        if exclude_states:
            query += " WHERE state NOT IN %(exclude)s"
            params["exclude"] = tuple(map(str, exclude_states))

        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, params)
            return [self._dict_to_job(dict(job)) for job in cursor]

    def cancel_job(self, job: Job) -> None:
        # FIXME: Check if job is running. A running job cannot be modified
        if not job.id:
            raise ValueError("Job %s has no ID" % job)
        if job.state in (JobState.finished, JobState.aborted):
            raise ValueError("Job %s is already in terminal state and cannot "
                             "be cancelled." % job)

        try:
            schema_manager = SchemaManager(self.db)  # type: ignore
            if job.temp_schema:
                schema_manager.drop_schema(job)
        except RuntimeError as e:
            LOG.error("Failed to delete the temporary schema: %s", e)
            raise
        else:
            job.temp_schema = None

        job.state = JobState.aborted
        self.store_job(job)

    def store_job(self, job: Job) -> None:
        if not isinstance(job, Job):
            raise TypeError("job to store must be a Job")

        props = OrderedDict(filter(
            lambda item: item[0] != "id", self._job_to_dict(job).items()))

        LOG.debug("Storing properties for job id %r: %s" % (job.id, props))

        query = sql.SQL("UPDATE job SET ({}) = %s WHERE id = %s") \
            .format(sql.SQL(", ").join(map(sql.Identifier, props)))
        query_params = (tuple(props.values()), job.id)

        with self.db.cursor() as cursor:
            cursor.execute(query, query_params)
        self.db.commit()

    def update_last_action(self, job: Job) -> None:
        if not isinstance(job, Job):
            raise TypeError("job must be a Job")

        with self.db.cursor() as cursor:
            cursor.execute(
                """
                UPDATE public.job
                SET id = id
                WHERE id = %s
                RETURNING last_action""", (job.id,))
            last_action = cursor.fetchone()[0]
        self.db.commit()

        job.last_action = last_action
        LOG.debug("Set last_action of %r to %s", job, last_action)

    def get_by_id(self, job_id: int) -> Job:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute("SELECT * FROM job WHERE id = %s", (job_id,))
            row = cursor.fetchone()

        if not row:
            raise ValueError(
                "There exists no import job with ID %d" % job_id)

        return self._dict_to_job(dict(row))

    def get_by_name(self, job_name: str) -> Job:
        with self.db.cursor() as cursor:
            cursor.execute("SELECT id FROM job WHERE name = %s", (job_name,))
            row = cursor.fetchone()

        if not row:
            raise ValueError(
                "There exists no import job with name %s" % job_name)

        return self.get_by_id(row[0])
