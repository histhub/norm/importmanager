import logging
import psycopg2  # type: ignore
import traceback

from datetime import datetime
from typing import List, Optional

from importmanager.db.log_manager import LogManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)
from importmanager.job import Job

LOG = logging.getLogger(__name__)


class PgSQLLogManager(LogManager):
    def __init__(self, state_db: StateDBConnection) -> None:
        if state_db.backend is not StateDBBackend.POSTGRESQL:
            raise TypeError(
                "state_db must be a PostgreSQL backed StateDBConnection")
        super().__init__(state_db)

    def log(self, job: Job, record: logging.LogRecord) -> None:
        trace = None
        if record.exc_info:
            trace = "".join(traceback.format_exception(*record.exc_info))

        with self.db.cursor() as cursor:
            psycopg2.extras.execute_values(
                cursor, """
                INSERT INTO log (
                  import_id, timestamp, logger, level, message, trace,
                  pathname, lineno
                )
                VALUES %s
                """, [(
                    job.id,
                    datetime.fromtimestamp(record.created),
                    record.name,
                    record.levelno,
                    record.getMessage(),
                    trace,
                    record.pathname,
                    record.lineno
                    )])

        self.db.commit()

    def tail_logs(self, job: Job,
                  level: int = logging.NOTSET,
                  limit: Optional[int] = None) -> List[logging.LogRecord]:
        with self.db.cursor(
                cursor_factory=psycopg2.extras.DictCursor) as cursor:
            query = """
            SELECT *
            FROM log
            WHERE import_id = %(import_id)s AND level >= %(level)s
            ORDER BY id DESC
            """
            params = dict(level=level, import_id=job.id)
            if limit and limit > 0:
                query += "LIMIT %(limit)s"
                params["limit"] = limit

            query = "SELECT * FROM (%s) rev ORDER BY id ASC" % query

            cursor.execute(query, params)
            return list(map(self._row_to_record, cursor))
