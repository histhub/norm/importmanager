import abc

from typing import (Optional, Type)

from importmanager.db.state_db import (StateDBBackend, StateDBConnection)


class SchemaMigrator(metaclass=abc.ABCMeta):
    _latest_version = 12

    def __new__(cls, state_db: StateDBConnection, debug: bool = False) \
            -> "SchemaMigrator":
        if cls is not SchemaMigrator:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        real_cls = None  # type: Optional[Type[SchemaMigrator]]
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from importmanager.db.pgsql.migrator import PgSQLSchemaMigrator
            real_cls = PgSQLSchemaMigrator

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, debug)

        raise NotImplementedError("This DB dialect is not supported")

    def __init__(self, state_db: StateDBConnection, debug: bool) -> None:
        self.state_db = state_db

    @abc.abstractmethod
    def create(self) -> None:
        """Creates and prepares the schema specified in the URL."""

    @abc.abstractmethod
    def create_schema(self) -> None:
        """Creates the schmema specified in the connection URL."""

    @abc.abstractmethod
    def schema_exists(self) -> bool:
        """Returns if the schema to be initialised exists.

        :returns: bool -- True if the schema exists in the RDBMS.
        """

    @abc.abstractmethod
    def is_created(self) -> bool:
        """Returns if the schema has been initialised.

        :returns: bool -- True if the schema has been initialised.
        """

    @abc.abstractmethod
    def destroy(self) -> None:
        """Destroys a schema by deleting it.

        NOTE: This method does not raise an exception if the schema does not
        exist.
        """

    @abc.abstractmethod
    def current_version(self) -> int:
        """Returns the current schema version.

        :returns: int -- the current schema version
        """

    def migration_needed(self, version: int = _latest_version) -> bool:
        """Returns whether a migration is needed to to get the schema to `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        :returns: bool -- True if a migration is needed.
        """
        return self.current_version() < version

    def migrate(self, version: int = _latest_version) -> None:
        """Migrate the schema to `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        """
        current = self.current_version()

        if current >= version:
            print("Schema is already at version %u. Nothing to do." % current)
            return

        if version > self._latest_version:
            raise ValueError(
                "There is no migration to version %u available!" % version)

        print("Schema is currently at version: %d" % current)

        for state_version in range(current, version):
            # Migrate to next version
            self.run_migration(state_version + 1)

    @abc.abstractmethod
    def run_migration(self, version: int) -> None:
        """Execute the migration scripts for a specific schema version.

        :param version: the schema version to execute migrations scripts for.
        :type version: int
        :raises: on errors.
        """
