import abc
import uuid

from typing import Any, Optional, Type

from importmanager.job import Job
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)


class SchemaManager(metaclass=abc.ABCMeta):
    def __new__(cls, state_db: StateDBConnection,
                *args: Any, **kwargs: Any) -> "SchemaManager":
        """Creates a new SchemaManager based on the provided DB connection.

        :param state_db: A StateDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: SchemaManager -- a new SchemaManager instance
        """

        if cls is not SchemaManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")

        real_cls = None  # type: Optional[Type[SchemaManager]]
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from .pgsql.schema_manager import PgSQLSchemaManager
            real_cls = PgSQLSchemaManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, *args, **kwargs)

        raise NotImplementedError("This DB type is not supported")

    @abc.abstractmethod
    def __init__(self, state_db: StateDBConnection) -> None:
        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")
        self.db = state_db

    @classmethod
    def _generate_schema_uuid(cls) -> str:
        return ("temp_%s" % ("_".join(map(
            lambda _: format(_, "x"), uuid.uuid4().fields))))

    @abc.abstractmethod
    def schema_for_job(self, job: Job) -> Optional[str]:
        """Get the name of the temporary schema for the given import job.

        :param job: The job
        :type job: Job
        :returns: Optional[str] -- name of the temporary schema or None if
        none has been created so far.
        """

    @abc.abstractmethod
    def load_model(self, job: Job, path: str) -> int:
        """Loads a model from a YAML file into a PostgreSQL schema.

        :param job: The job into those temp. schema the model should be loaded
        :param path: Path to the YAML file containing the model definition
        :returns: int -- the ID of the newly created model
        :raises: RuntimeError if an error occurs
        """

    @abc.abstractmethod
    def generate_schema_for_job(self, job: Job) -> str:
        """Create a new temporary schema for the given import job.

        :param job: The job
        :type job: Job
        :returns: str -- name of the created schema
        :raises: RuntimeError if the job already has a temporary schema
                  associated
        """

    @abc.abstractmethod
    def drop_schema(self, job: Job) -> None:
        """Drops the temporary schema associated with job.

        :param job: The job_dict
        :type job: Job
        :raises: ValueError if the job does not have a temporary schema
                  associated
        """
