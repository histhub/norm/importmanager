import abc

from typing import (Any, Dict, Generator, List, Optional, Sequence, Tuple)

from importmanager.concordance import (Concordance, ConcordanceState)
from importmanager.job import Job
from importmanager.db.job_manager import JobManager
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)


class ConcordanceManager(metaclass=abc.ABCMeta):
    def __new__(cls, state_db: StateDBConnection,
                *args: Any, **kwargs: Any) -> "ConcordanceManager":
        """Creates a new ConcordanceManager based on the provided DB connection.

        :param state_db: A StateDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: ConcordanceManager -- a new ConcordanceManager instance
        """

        if cls is not ConcordanceManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")

        real_cls = None
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from .pgsql.concordance_manager import PgSQLConcordanceManager
            real_cls = PgSQLConcordanceManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    @abc.abstractmethod
    def __init__(self, state_db: StateDBConnection) -> None:
        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")
        self.db = state_db
        self.job_manager = JobManager(self.db)  # type: ignore

    def _dict_to_concordance(self, concordance_dict: Dict[str, Any]) \
            -> Concordance:
        """Create a Concordance object from a dict from the DB

        :param concordance_dict: the dict to convert
        :returns: Concordance"""

        def _argify(concordance_dict: Dict[str, Any]) \
                -> Generator[Tuple[str, Any], None, None]:
            for k, v in concordance_dict.items():
                if k == "import_id":
                    k = "job"
                    v = self.job_manager.get_by_id(v)
                elif k == "state":
                    v = ConcordanceState(v)

                yield (k, v)

        args = dict(_argify(concordance_dict))
        return Concordance(**args)

    def _concordance_to_dict(self, concordance: Concordance) -> Dict[str, Any]:
        """Convert a Concordance object to a dict that can be stored in the
        DB

        :param concordance: the concordance to convert
        :type concordance: Concordance
        :returns: dict -- the resulting dict"""

        def _conv(concordance: Concordance) -> Generator[
                Tuple[str, object], None, None]:
            for k, v in concordance:
                if k == "job":
                    k = "import_id"
                    v = v.id
                elif k == "state":
                    v = str(v)

                yield (k, v)

        return dict(_conv(concordance))

    @abc.abstractmethod
    def fetch_concordance(self, job: Job, id: int) -> Concordance:
        """Fetches a concordances from the state database.

        :param job: the job for which concordances should be fetched.
        :type job: Job
        :param id: the database ID of the concordance to fetch
        :type id: int
        :returns: Concordance -- the concordances
        :raises RuntimeError: when no such concordance exists
        """

    @abc.abstractmethod
    def list_concordances(self, job: Job, limit: Optional[int] = None,
                          include_resolved: bool = False) -> List[Concordance]:
        """Fetches a list of all available concordances in the state database.

        :param job: the job for which concordances should be fetched.
        :type job: Job
        :param limit: the maximum number of results
        :type limit: Optional[int]
        :param include_resolved: flag whether to include concordances that
        have already been marked as resolved
        :type include_resolved: bool
        :returns: List[Concordance] -- the list of concordances
        """

    @abc.abstractmethod
    def count_concordances(self, job: Job,
                           include_resolved: bool = False) -> int:
        """Counts the number of stored concordances for a given job.

        :param job: the job for which concordances should be counted.
        :type job: Job
        :param include_resolved: flag whether to include concordances that
        have already been marked as resolved
        :type include_resolved: bool
        :returns: int -- number of stored concordances
        """

    def delete_concordance(self, concordance: Concordance) -> None:
        """Delete a given concordance from the state database.

        Note: The concordance's id property will be set to None.

        :param concordance: the concordance to be deleted.
        :type concordance: Concordance
        :raises RuntimeError: when the deletion failed.
        :raises ValueError: when the concordance does not have an ID set.
        """
        if not concordance or concordance.id is None:
            raise ValueError("Concordance %r has no ID set." % concordance)

        if self.delete_concordances((concordance,)) < 1:
            raise RuntimeError(
                "The deletion of concordance %r failed." % concordance)

    @abc.abstractmethod
    def delete_concordances(self, concordances: Sequence[Concordance]) -> int:
        """Delete multiple concordances from the state database.

        Note: The concordances' id properties will be set to None.

        :param concordances: the concordances to be deleted.
        :type concordances: Sequence[Concordance]
        :returns: int -- the number of deleted concordances.
        """

    @abc.abstractmethod
    def store_concordance(self, concordance: Concordance) -> None:
        """Store a new or an updated Concordance object to the state DB.

        This method will do nothing, if concordance.id is set but no
        concordance exists with a matching ID exists in the state DB.
        Moreover, it will try to prevent the creation of duplicate
        (import_id, staging_id) pairs in the state DB.

        :param concordance: the concordance to store
        :type concordance: Concordance
        :raise ValueError: if the concordance.id does not match
        a (import_id, staging_id) pair already present in the state DB.
        """

    @abc.abstractmethod
    def store_concordances(self, concordances: Sequence[Concordance]) -> None:
        """Store an updated list of Concordance object to the database.
        This method will ignore concordances without a matching ID.

        Note: Some concordances might've been stored in the state DB even if an
        exception is raised.
        This method might update already existing concordances in the state DB
        if the concordance's ID is None.

        :param concordances: the sequence of concordances to store.
        :type concordance: Sequence[Concordance]
        :raises ValueError: if any of the concordances' IDs does not match
        a (import_id, staging_id) pair already present in the state DB.
        """
