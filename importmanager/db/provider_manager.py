import abc

from typing import Any, List, Optional, Type

from importmanager.provider import Provider
from importmanager.db.state_db import (StateDBBackend, StateDBConnection)


class ProviderManager(metaclass=abc.ABCMeta):
    def __new__(cls, state_db: StateDBConnection,
                *args: Any, **kwargs: Any) -> "ProviderManager":
        """Creates a new ProviderManager based on the provided DB connection.

        :param state_db: A StateDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: a new ProviderManager instance
        """

        if cls is not ProviderManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            pm = object.__new__(cls)  # type: ProviderManager
            return pm

        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")

        real_cls = None  # type: Optional[Type[ProviderManager]]
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from .pgsql.provider_manager import PgSQLProviderManager
            real_cls = PgSQLProviderManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from .sqlite.provider_manager import SQLiteProviderManager
        #     real_cls = SQLiteProviderManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, *args, **kwargs)

        raise NotImplementedError("This DB type is not supported")

    @abc.abstractmethod
    def __init__(self, state_db: StateDBConnection) -> None:
        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")
        self.db = state_db

    @abc.abstractmethod
    def create_provider(self, name: str) -> Provider:
        """Creates a new Provider.

        :param name: The name of the Provider
        :type name: str
        :returns: Provider -- the newly created provider
        """

    @abc.abstractmethod
    def list_providers(self) -> List[Provider]:
        """Fetches a list of all available Providers in the state database.

        :returns: List[Provider] -- the list of Providers
        """

    @abc.abstractmethod
    def get_by_id(self, provider_id: int) -> Provider:
        """Fetches a Provider from the state database by ID

        :param provider_id: the ID to fetch
        :type provider_id: int
        :returns: Provider -- the provider (when found)
        :raises ValueError: if no provider with `provider_id` exists
        """

    @abc.abstractmethod
    def get_by_name(self, provider_name: str) -> Provider:
        """Fetches a Provider from the state database by name

        :param provider_name: the name to fetch
        :type provider_name: str
        :returns: Provider -- the provider (when found)
        :raises ValueError: if no provider with `provider_name` exists
        """
