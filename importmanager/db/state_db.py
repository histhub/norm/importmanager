import enum

from eavlib.db.url import URL
from typing import Any

StateDBBackend = enum.Enum("StateDBBackend", (
    "POSTGRESQL",
    # "SQLITE",
    ))


class StateDBConnection:
    def __init__(self, url: URL) -> None:
        """Creates a new StateDBConnection based on the provided URL.

        :param url: A database connection URL
        :raises NotImplementedError: when the DB dialect is not (yet) supported
        """

        self.url = url
        dialect = url.get_backend_name()

        if dialect == "postgresql":
            import psycopg2  # type: ignore
            self.backend = StateDBBackend.POSTGRESQL
            self.connection = psycopg2.connect(**url.translate_connect_args())
            self.connection.set_client_encoding("UTF8")
            if url.schema and url.schema != "public":
                raise ValueError("URL schema must be 'public'!")
        # elif dialect == "sqlite":
        #     import sqlite3
        #     self.connection = ...
        else:
            raise NotImplementedError(
                "DB dialect %s is not supported" % dialect)

    def copy(self) -> "StateDBConnection":
        return StateDBConnection(self.url)

    def cursor(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.cursor(*args, **kwargs)

    def commit(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.commit(*args, **kwargs)

    def rollback(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.rollback(*args, **kwargs)

    def close(self) -> Any:
        return self.connection.close()
