import abc
import collections.abc
import logging

from typing import Any, List, Optional, Type

from importmanager.db.state_db import (StateDBBackend, StateDBConnection)
from importmanager.job import Job


class LogManager(metaclass=abc.ABCMeta):
    def __new__(cls, state_db: StateDBConnection,
                *args: Any, **kwargs: Any) -> "LogManager":
        """Creates a new LogManager based on the provided DB connection.

        :param state_db: A StateDBConnection
        :raises NotImplementedError: when the DB dialect is not (yet) supported
        :returns: a new LogManager instance
        """

        if cls is not LogManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            lm = object.__new__(cls)  # type: LogManager
            return lm

        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")

        real_cls = None  # type: Optional[Type[LogManager]]
        if state_db.backend is StateDBBackend.POSTGRESQL:
            from .pgsql.log_manager import PgSQLLogManager
            real_cls = PgSQLLogManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from .sqlite.log_manager import SQLiteLogManager
        #     real_cls = SQLiteLogManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, state_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    @abc.abstractmethod
    def __init__(self, state_db: StateDBConnection) -> None:
        if not isinstance(state_db, StateDBConnection):
            raise TypeError("state_db must be a StateDBConnection")
        self.db = state_db

    def _row_to_record(self, row: "collections.abc.Mapping[str, Any]") \
            -> logging.LogRecord:
        record = logging.LogRecord(
            name=row["logger"], level=row["level"], pathname=row["pathname"],
            lineno=row["lineno"], msg=row["message"], args=(), exc_info=None)
        record.created = row["timestamp"].timestamp()
        record.msecs = row["timestamp"].microsecond / 1000
        return record

    @abc.abstractmethod
    def log(self, job: Job, record: logging.LogRecord) -> None:
        """Inserts another LogRecord into the database.

        :param job: The job this log record is for
        :type job: Job
        :param record: The LogRecord to insert.
        :type name: LogRecord
        """

    @abc.abstractmethod
    def tail_logs(self, job: Job,
                  level: int = logging.NOTSET, limit: Optional[int] = None) \
            -> List[logging.LogRecord]:
        """Fetches the last `limit` LogRecords for a `job` with level
        gte `level`.

        :param job: The job to fetch LogRecords for
        :type job: Job
        :param level: The minimum level a record must have to be selected
        :type level:
        :param limit: The maximum number of records to return
        :type limit: int
        :returns: List[LogRecord] -- the list of LogRecords
        """
