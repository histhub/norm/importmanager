import os

from setuptools import setup, find_packages


try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


base = os.path.abspath(os.path.dirname(__file__))


def read_file(path):
    """Read file and return contents"""
    try:
        with open(path, encoding="utf-8") as f:
            return f.read()
    except FileNotFoundError:
        return ""


def get_reqs(target):
    """Parse requirements.txt files and return array"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.d", ("%s.txt" % target)),
        session="hack")
    return [
        r.requirement if hasattr(r, "requirement") else str(r.req)
        for r in reqs]


def get_meta(name):
    """Get metadata from importmanager/__init__.py"""
    import importmanager
    return getattr(importmanager, name)


setup(
    name="importmanager",
    version=get_meta("__version__"),
    license=get_meta("__license__"),
    author=get_meta("__author__"),

    description=get_meta("__description__"),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Intended Audience :: Developers",
        "Typing :: Typed"
    ],

    # platforms=["mac", "linux"],

    package_data={"importmanager": ["py.typed", "files/db/*/*.sql"]},

    # scripts=[],

    # provides=[],
    install_requires=get_reqs("importmanager"),
    extras_require=dict(dev=get_reqs("development")),

    # namespace_packages=[],
    packages=find_packages(exclude=["importmanager.tests"]),
    python_requires="~=3.6",
    # include_package_data=True,

    zip_safe=True)
