# Variables
SRC_DIR := ./importmanager
FRONTENDS := cli
# MIN_PYTHON_VERSION: major.minor.micro allowed
MIN_PYTHON_VERSION := 3.6

export MIN_PYTHON_VERSION

# OS detection
ifeq ($(OS),Windows_NT)
$(error Windows NT is not supported. Things are going to break)
endif

# Check if Python binary matches minimum required version
# Usage: $(call is_min_python_version,/path/to/python,min_version_str)
# prints 1 if greater or equal, nothing
define is_min_python_version
$(shell echo $(2) | $(1) -c 'import sys; min = tuple(map(int, sys.stdin.readline().split("."))); sys.stdout.write("1" * (sys.version_info[:len(min)] >= min))')
endef

# Get version of Python binary (e.g. 2.7.15, 3.7.0, 3.8.0)
# Usage: $(call python_version,/path/to/python)
define python_version
$(shell $(1) -c 'import sys; sys.stdout.write(".".join([str(x) for x in sys.version_info[:3]]))')
endef

# Path detection
export BASE_DIR := $(shell dirname '$(realpath $(lastword $(MAKEFILE_LIST)))')
export VENV := $(BASE_DIR)/venv

# Determine which Python binary to use
ifneq ($(PYTHON),)
SYSTEMPYTHON := $(PYTHON)
else
SYSTEMPYTHON := $(firstword $(shell command -v python3 python ;))
endif

export VENVPYTHON := $(VENV)/bin/python

# Unset PYTHONHOME environment variable, because that's what a virtualenv's
# activate script does, too
unexport PYTHONHOME

# Helper command to install using pip into the virtualenv
export PIP_INSTALL = \
$(if $(CFLAGS),CFLAGS='$(CFLAGS)') \
$(if $(LDFLAGS),LDFLAGS='$(LDFLAGS)') \
$(if $(ARCHFLAGS),ARCHFLAGS='$(ARCHFLAGS)') \
'$(VENVPYTHON)' -m pip install $(PIPFLAGS) --disable-pip-version-check


.PHONY: frontends/%
$(foreach fe,$(FRONTENDS),frontends/$(fe)/%):
	$(MAKE) -C '$(BASE_DIR)/$(patsubst %/,%,$(dir $@))' '$(notdir $@)'


ifneq ($(BASE_DIR),$(shell pwd -P))
# Ensure that all commands are executed in the BASE_DIR.
%: ; $(MAKE) -C '$(BASE_DIR)' $@
else

# Help

.PHONY: help
help:
	$(info )
	$(info Welcome to ImportManager.)
	$(info )
	$(info You can choose between these targets:)
	$(info )
	$(info * help				Display this help page.)
	$(info )
	$(info ImportManager:)
	$(info * virtualenv			Create a virtual environment.)
	$(info * clean				Clean up.)
	$(info )
	$(info Development:)
	$(info * lint				Run the linter.)
	$(info * test				Run the tests.)
	$(info * type_check			Run the MyPy type checker.)
	$(info )
	@: # noop (needed to suppress "nothing to be done")


# Virtualenv targets

$(VENV):
ifndef SYSTEMPYTHON
	$(error Cannot find Python. Please make sure that Python is in PATH, or set
	        the PYTHON envionment variable to the path to your Python
	        interpreter.)
endif
# Check Python version
ifeq ($(call is_min_python_version,$(SYSTEMPYTHON),$(MIN_PYTHON_VERSION)),)
	$(error Python version $(MIN_PYTHON_VERSION) required. Only $(call python_version,$(SYSTEMPYTHON)) found)
endif
# Configure virtualenv
	$(SYSTEMPYTHON) -m venv '$(VENV)'

.PRECIOUS: $(VENV)/.%_reqs
$(VENV)/.fe_%_reqs: | $(VENV)
	$(MAKE) -C 'frontends/$*' install_reqs
$(VENV)/.%_reqs: requirements.d/%.txt | $(VENV)
# Install requirements for %
	$(PIP_INSTALL) -r './requirements.d/$*.txt'
	@touch '$@'


# PHONY targets

.PHONY: virtualenv
virtualenv: | $(VENV) $(VENV)/.importmanager_reqs $(foreach fe,$(FRONTENDS),$(VENV)/.fe_$(fe)_reqs)
	$(VENV)/bin/python -m pip install -e .
	$(foreach fe,$(FRONTENDS),$(VENV)/bin/python -m pip install -e frontends/$(fe))
	$(info Created virtualenv in "$(realpath $(VENV))".)


.PHONY: clean
clean:
	$(RM) -R '$(VENV)'
	-$(RM) -R build dist
	-$(RM) -R importmanager.egg-info
	-$(RM) .coverage
	-$(RM) -R .mypy_cache
	-find '$(SRC_DIR)' -depth -iname '*.pyc' -exec $(RM) -R '{}' \;
	-find '$(SRC_DIR)' -depth -type d -name '__pycache__' -exec $(RM) -R '{}' \;
	@$(foreach fe,$(FRONTENDS),$(MAKE) -C 'frontends/$(fe)' clean ;)

.PHONY: lint
.ONESHELL: lint
lint: | $(VENV)/.development_reqs
	@rc=0
	@$(VENVPYTHON) -m flake8 --show-source '$(SRC_DIR)' || rc=2
	@$(foreach fe,$(FRONTENDS),$(MAKE) -s -w -C 'frontends/$(fe)' lint || rc=2 ;)
	@exit $$rc

.PHONY: test test_importmanager
test:
	@rc=0
	@$(MAKE) test_importmanager || rc=2
	$(foreach fe,$(FRONTENDS),$(MAKE) -s -w -C 'frontends/$(fe)' test || rc=2 ;)
	@exit $$rc

test_importmanager: | $(VENV)/.importmanager_reqs $(VENV)/.development_reqs
	$(VENVPYTHON) -m coverage run --branch --omit='importmanager/tests' -m unittest discover --verbose -p 'test_*.py' tests.importmanager
	$(VENVPYTHON) -m coverage report --show-missing --include='importmanager/*'

.PHONY: type_check
type_check: | $(VENV)/.development_reqs
	@rc=0
	@$(VENVPYTHON) -m mypy --strict --python-version $(MIN_PYTHON_VERSION) \
		--allow-untyped-calls '$(SRC_DIR)' || rc=2
	$(foreach fe,$(FRONTENDS),$(MAKE) -s -w -C 'frontends/$(fe)' type_check || rc=2 ;)
	@exit $$rc

endif
