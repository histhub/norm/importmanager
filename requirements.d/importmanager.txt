psycopg2>=2.7 --no-binary=psycopg2
eavlib@git+https://gitlab.com/histhub/norm/eavlib.git@v0.3.1
normlib@git+https://gitlab.com/histhub/norm/normlib.git@v1.0.1
typing;python_version<"3.5"

# Modules
tsv_loader@git+https://gitlab.com/histhub/norm/tsv-loader.git@v1.0
importtools@git+https://gitlab.com/histhub/norm/importtools.git@v0.2
