import unittest

from importmanager.job import Job, JobState, JobStep
from importmanager.provider import Provider


class JobTest(unittest.TestCase):
    def setUp(self) -> None:
        self._id = 123
        self._name = "first import"
        self._provider = Provider("SSRQ")

        self.job = Job(self._name, self._provider, id=self._id)

    def test_init(self) -> None:
        assert self._id == self.job.id
        assert self._name == self.job.name
        assert self._provider == self.job.provider
        assert JobStep.none == self.job.step
        assert JobState.active == self.job.state

    def test_eq(self) -> None:
        j = Job("demo", self._provider)

        assert self.job != j
        assert self.job != dict(self.job)
        # copy_job = Job(**dict(self.job))
        # assert self.job == copy_job

        assert j != 0
        assert j != "a str"
        assert j != list()
        assert j != dict()

    def test_iter(self) -> None:
        assert tuple(self.job) == (
            ("id", self._id),
            ("name", self._name),
            ("provider", self._provider),
            ("state", JobState.active),
            ("step", JobStep.none),
            ("next_step", JobStep.load),
            ("created_at", self.job.created_at),
            ("last_action", None),
            ("temp_schema", None),
            ("model_id", None),
            ("error", None)
        )

    def test_str(self) -> None:
        assert self._name == str(self.job)


class JobStateTest(unittest.TestCase):
    def test_str(self) -> None:
        for value, state in JobState.__members__.items():
            assert value == str(state)


class JobStepTest(unittest.TestCase):
    def test_str(self) -> None:
        for value, state in JobStep.__members__.items():
            assert value == str(state)
