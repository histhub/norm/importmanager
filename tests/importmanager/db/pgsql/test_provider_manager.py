import psycopg2
import random
import string
import unittest

from itertools import islice
from typing import Any, Callable, Iterator, Optional, Tuple
from unittest.mock import Mock

from importmanager.db.pgsql.provider_manager import PgSQLProviderManager
from importmanager.provider import Provider

from .. import providers


def _provider_by_name(name: str) -> Optional[Provider]:
    try:
        return next(filter(lambda _: name == getattr(_, "name"),
                           providers.values()))
    except StopIteration:
        return None


def _rand_nonexist_prov_attr(attr: str, randgen: Callable[[], Any]) \
        -> Iterator[Any]:
    provider_attrs = tuple(map(lambda _: getattr(_, attr), providers.values()))
    while True:
        rand = randgen()
        if rand not in provider_attrs:
            yield rand


class PgSQLProviderManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_conn = Mock(spec=psycopg2.extensions.connection)
        self.provider_manager = PgSQLProviderManager(self.mock_conn)

    def test_create_provider(self) -> None:
        provider_id = 123
        name = "SSRQ"

        self.mock_conn.cursor.return_value.fetchone.return_value = \
            (provider_id,)

        provider = self.provider_manager.create_provider(name)

        assert self.mock_conn.cursor.return_value.execute.called
        self.mock_conn.cursor.return_value.execute.assert_called_with(
            "INSERT INTO provider (name) VALUES (%s) "
            "RETURNING id", (name,))
        assert self.mock_conn.commit.called

        assert provider is not None
        assert isinstance(provider, Provider)
        assert name == provider.name
        assert provider_id == provider.id

    def test_list_providers(self) -> None:
        self.mock_conn.cursor.return_value.fetchall.return_value = \
            [dict(_) for _ in providers.values()]

        provider_list = self.provider_manager.list_providers()
        assert list(providers.values()) == provider_list

    def test_get_by_id(self) -> None:
        # Check valid invocations
        for desired_provider in providers.values():
            def _exec_mock(query: str, params: Tuple[Any]) -> None:
                cursor_mock = self.mock_conn.cursor.return_value

                if "SELECT id, name FROM provider WHERE id = %s" == query:
                    try:
                        _provider = providers[params[0]]
                        cursor_mock.fetchone.return_value = dict(
                            id=_provider.id, name=_provider.name)
                    except KeyError:
                        cursor_mock.fetchone.return_value = None

            self.mock_conn.cursor.return_value.execute.side_effect = _exec_mock

            provider = self.provider_manager.get_by_id(desired_provider.id)  # type: ignore
            assert desired_provider == provider

        # Check with non existant IDs
        for id in islice(_rand_nonexist_prov_attr(
                "id", lambda: random.randint(1, 1000)), 5):
            with self.assertRaises(ValueError):
                self.provider_manager.get_by_id(id)

    def test_get_by_name(self) -> None:
        # Check valid invocations
        for desired_provider in providers.values():
            def _exec_mock(query: str, params: Tuple[Any]) -> None:
                cursor_mock = self.mock_conn.cursor.return_value

                _provider = None  # type: Optional[Provider]
                if "SELECT id, name FROM provider WHERE id = %s" == query:
                    try:
                        _provider = providers[params[0]]
                        cursor_mock.fetchone.return_value = dict(
                            id=_provider.id, name=_provider.name)
                    except KeyError:
                        cursor_mock.fetchone.return_value = None
                elif "SELECT id FROM provider WHERE name = %s" == query:
                    _provider = _provider_by_name(params[0])
                    cursor_mock.fetchone.return_value = \
                        (_provider.id,) if _provider else None

            self.mock_conn.cursor.return_value.execute.side_effect = _exec_mock

            provider = self.provider_manager.get_by_name(desired_provider.name)
            assert desired_provider == provider

        # Check with non existant names
        def _randname() -> str:
            s = string.ascii_lowercase + string.digits
            return "".join(random.sample(s, 10))

        for name in islice(_rand_nonexist_prov_attr(
                "name", _randname), 5):
            with self.assertRaises(ValueError):
                self.provider_manager.get_by_name(name)
