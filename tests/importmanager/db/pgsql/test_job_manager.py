import psycopg2
import random
import string
import unittest

from itertools import islice
from typing import Any, Callable, Iterator, Optional, Tuple, TypeVar
from unittest.mock import Mock, patch

from importmanager.job import Job, JobState, JobStep
from importmanager.db.pgsql.job_manager import PgSQLJobManager

from .. import jobs, providers, job_list_to_db_rows

T = TypeVar("T")


def next_or_none(iter: Iterator[T]) -> Optional[T]:
    try:
        return next(iter)
    except StopIteration:
        return None


def _rand_nonexist_job_attr(attr: str, randgen: Callable[[], Any]) \
        -> Iterator[Any]:
    job_attrs = tuple(map(lambda job: getattr(job, attr), jobs))
    while True:
        rand = randgen()
        if rand not in job_attrs:
            yield rand


def _job_by(val: Any, attr: str) -> Optional[Job]:
    try:
        return next(filter(lambda job: val == getattr(job, attr), jobs))
    except StopIteration:
        return None


class PgSQLJobManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_conn = Mock(spec=psycopg2.extensions.connection)
        with patch("importmanager.db.job_manager.ProviderManager") as mock_pmr:
            self.job_manager = PgSQLJobManager(self.mock_conn)
            self.mock_provider_manager = mock_pmr

        self.mock_provider_manager.return_value.get_by_id.side_effect = \
            lambda id: providers.get(id, None)

    def test_create_job(self) -> None:
        name = "Test job"
        provider = providers[1]
        job_id = 123

        self.mock_conn.cursor.return_value.fetchone.return_value = \
            (job_id, "active", "load")

        job = self.job_manager.create_job(name, provider)

        # Check that the job has been inserted into the DB
        assert self.mock_conn.cursor.return_value.execute.called
        self.mock_conn.cursor.return_value.execute.assert_called_with(
            "INSERT INTO job (name, provider_id, step) VALUES (%s, %s, %s) "
            "RETURNING id, state, step", (name, provider.id, "load"))

        assert self.mock_conn.commit.called

        assert job is not None
        assert isinstance(job, Job)
        assert name == job.name
        assert provider == job.provider
        assert JobState.active == job.state
        assert JobStep.load == job.step
        assert job_id == job.id

    def test_list_jobs(self) -> None:
        self.mock_conn.cursor.return_value.fetchall.return_value = \
            list(job_list_to_db_rows(jobs))

        assert jobs == self.job_manager.list_jobs()

    def test_cancel_job(self) -> None:
        job = jobs[1]

        self.job_manager.cancel_job(job)

        self.mock_conn.cursor.return_value.execute.assert_called_with(
            "UPDATE job SET state = 'aborted' WHERE id = %s", (job.id,))
        assert self.mock_conn.commit.called

        # Test "non-DB" Job
        job = Job("Non-DB job", providers[1])
        with self.assertRaises(ValueError):
            self.job_manager.cancel_job(job)

        # TODO: Should test with Job that has an ID but is not in the database

    def test_get_by_id(self) -> None:
        # Check valid invocations
        for desired_job in jobs:
            def _exec_mock(query: str, params: Tuple[Any]) -> None:
                cursor_mock = self.mock_conn.cursor.return_value

                if "SELECT * FROM job WHERE id = %s" == query:
                    _job = _job_by(params[0], "id")
                    rows = job_list_to_db_rows([_job])
                    cursor_mock.fetchone.side_effect = \
                        lambda: next_or_none(rows)

            self.mock_conn.cursor.return_value.execute.side_effect = _exec_mock

            job = self.job_manager.get_by_id(desired_job.id)  # type: ignore
            assert desired_job == job

        # Check with non existant IDs
        for id in islice(_rand_nonexist_job_attr(
                "id", lambda: random.randint(1, 1000)), 5):
            with self.assertRaises(ValueError):
                self.job_manager.get_by_id(id)

    def test_get_by_name(self) -> None:
        # Check valid invocations
        for desired_job in jobs:
            def _exec_mock(query: str, params: Tuple[Any]) -> None:
                cursor_mock = self.mock_conn.cursor.return_value

                if "SELECT * FROM job WHERE id = %s" == query:
                    _job = _job_by(params[0], "id")
                    rows = job_list_to_db_rows([_job])
                    cursor_mock.fetchone.side_effect = \
                        lambda: next_or_none(rows)
                elif "SELECT id FROM job WHERE name = %s" == query:
                    _job = _job_by(params[0], "name")
                    rows = job_list_to_db_rows([_job])

                    def _fetchone() -> Optional[Tuple[Any]]:
                        try:
                            return (next(rows).get("id"),)
                        except StopIteration:
                            return None
                    cursor_mock.fetchone.side_effect = _fetchone

            self.mock_conn.cursor.return_value.execute.side_effect = _exec_mock

            job = self.job_manager.get_by_name(desired_job.name)
            assert desired_job == job

        # Check with non existant names
        def _randname() -> str:
            s = string.ascii_lowercase + string.digits
            return "".join(random.sample(s, 10))

        for name in islice(_rand_nonexist_job_attr("name", _randname), 5):
            with self.assertRaises(ValueError):
                self.job_manager.get_by_name(name)
