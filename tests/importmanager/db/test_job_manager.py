import psycopg2
import unittest

from unittest.mock import Mock, patch

from importmanager.provider import Provider
from importmanager.db.job_manager import JobManager
from importmanager.db.pgsql.job_manager import PgSQLJobManager

from . import jobs, providers, job_list_to_db_rows


class JobManagerTest(unittest.TestCase):
    def test_new(self) -> None:
        with self.assertRaises(TypeError):
            JobManager(None)
        with self.assertRaises(TypeError):
            JobManager("not a DB connection")

        # PostgreSQL
        pgsql_conn = Mock(spec=psycopg2.extensions.connection)
        jm = JobManager(pgsql_conn)
        assert PgSQLJobManager == type(jm)

    def test_dict_to_job(self) -> None:
        for job in jobs:
            job_dict = next(job_list_to_db_rows([job]))
            reconstructed_job = self.job_manager._dict_to_job(
                job_dict)
            assert reconstructed_job is not None
            assert reconstructed_job == job

