from typing import Any, Dict, Iterator, Optional, Sequence

from importmanager.job import Job, JobState, JobStep
from importmanager.provider import Provider


providers = {
    1: Provider("SSRQ", id=1),
    2: Provider("foo", id=2)
}

jobs = [
    Job(id=1, name="foo", provider=providers[1],
        state=JobState.finished, step=JobStep.write),
    Job(id=13, name="bar", provider=providers[2],
        state=JobState.aborted, step=JobStep.load),
    Job(id=42, name="baz", provider=providers[1],
        state=JobState.active, step=JobStep.link)
]


def job_list_to_db_rows(job_list: Sequence[Optional[Job]]) \
        -> Iterator[Dict[str, Any]]:
    for job in job_list:
        if job:
            row = dict(job)
            row["provider_id"] = row["provider"].id
            del row["provider"]
            row["state"] = row["state"].value
            row["step"] = row["step"].value
            yield row
