import psycopg2
import unittest

from unittest.mock import Mock

from importmanager.db.provider_manager import ProviderManager
from importmanager.db.pgsql.provider_manager import PgSQLProviderManager


class ProviderManagerTest(unittest.TestCase):
    def test_new(self):
        with self.assertRaises(TypeError):
            ProviderManager(None)
        with self.assertRaises(TypeError):
            ProviderManager("not a DB connection")

        # PostgreSQL
        # pgsql_conn = Mock(spec=psycopg2.extensions.connection)
        # pm = ProviderManager(pgsql_conn)
        # assert type(pm) is PgSQLProviderManager
