import unittest

from typing import Any, Dict

from importmanager.provider import Provider


def _test_provider(id: int, name: str) -> Dict[str, Any]:
    return dict(id=id, name=name, provider=Provider(name, id=id))


providers = (
    _test_provider(id=1, name="SSRQ"),
    _test_provider(id=3, name=""),
    _test_provider(id=0, name="nullprovider"),
    _test_provider(id=-1, name="oops"),
)


class ProviderTest(unittest.TestCase):
    def test_init(self) -> None:
        for provider in providers:
            assert provider["id"] == provider["provider"].id
            assert provider["name"] == provider["provider"].name

    def test_eq(self) -> None:
        p = Provider("demo")
        for provider in providers:
            assert provider["provider"] != p
            assert provider["provider"] != dict(provider["provider"])
            copy_provider = Provider(**dict(provider["provider"]))
            assert provider["provider"] == copy_provider

        assert p != 0
        assert p != "a str"
        assert p != list()
        assert p != dict()

    def test_str(self) -> None:
        for provider in providers:
            assert provider["name"] == str(provider["provider"])

    def test_iter(self) -> None:
        for provider in providers:
            assert [
                ("id", provider["id"]),
                ("name", provider["name"])
            ] == list(provider["provider"])

    def test_id(self) -> None:
        for provider in providers:
            assert provider["id"] == provider["provider"].id

    def test_name(self) -> None:
        for provider in providers:
            assert provider["name"] == provider["provider"].name
